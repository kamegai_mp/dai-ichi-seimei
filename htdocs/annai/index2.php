<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>第一生命ホール</title>

<SCRIPT language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</SCRIPT>

</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('../img_cmn/navi01_2.gif','../img_cmn/navi02_2.gif','../img_cmn/navi04_2.gif','../img_cmn/navi05_2.gif','../img_cmn/navi06_2.gif')">
<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
    <td width="779">
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
        </tr>
        <tr>
          <td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
        </tr>
        <tr>
          <td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
        </tr>
        <tr>
          <td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" border="0"></a></td>
        </tr>
      </table>
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><a href="../history/index.html" onMouseOver="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
          <td><a href="../about-hall/index.html" onMouseOver="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
          <td><img src="../img_cmn/navi03_2.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></td>
          <td><a href="../seatplan/index.html" onMouseOver="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
          <td><a href="../schedule/index.php" onMouseOver="MM_swapImage('navi05','','../img_cmn/navi05_2.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../img_cmn/navi05_1.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></a></td>
          <td><a href="../access/index.html" onMouseOver="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
          <td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
        </tr>
      </table>
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="637"><table width="370" height="88" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="36"><img src="img/spacer.gif" width="10" height="36"></td>
            </tr>
            <tr>
              <td height="52" align="right" valign="top"><img src="img/annai_01.gif" alt="ご利用案内 How to rent the hall" width="345" height="21"></td>
            </tr>
          </table></td>
          <td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
        </tr>
      </table>
      <table width="669" height="48" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="23" valign="top"><img src="img/annai_02.gif" alt="第一生命ホールのご利用をお考えですか？" width="250" height="14"></td>
        </tr>
        <tr>
          <td height="25" valign="top"><font color="#003366" class="text1">このホールを使って、演奏会をご計画でしたら、まずお問いあわせ下さい。</font></td>
        </tr>
      </table>

      <table width="669" height="173" border="0" align="center" cellpadding="0" cellspacing="0" background="img/annai_bg.gif">
        <tr>
          <td align="right"><table width="664" height="173" border="0" cellpadding="0" cellspacing="5" background="img/spacer.gif">
            <tr>
              <td width="136" height="29" valign="middle"><img src="img/spacer.gif" width="1" height="1"></td>
              <td width="170" align="center" valign="middle"><font color="#FFFFFF" class="text4">午前の部<br>（9：00&#65374;12：00）</font></td>
              <td width="167" align="center" valign="middle"><font color="#FFFFFF" class="text4">午後の部<br>（13：00&#65374;17：00）</font></td>
              <td width="166" align="center" valign="middle"><font color="#FFFFFF" class="text4">夜間の部<br>（18：00&#65374;21：30）</font></td>
            </tr>
            <tr>
              <td height="33" valign="middle"><font color="#FFFFFF" class="text4" style="font-weight:bold;">各区分基本料金</font></td>
              <td align="center" valign="middle"><font color="#333333" class="text4" style="font-weight:bold;">200,000円</font></td>
              <td align="center" valign="middle"><font color="#333333" class="text4" style="font-weight:bold;" >300,000円</font></td>
              <td align="center" valign="middle"><font color="#333333" class="text4" style="font-weight:bold;">400,000円</font></td>
            </tr>
            <tr>
              <td colspan="4" height="16"><font color="#FFFFFF" class="text4" style="font-weight:bold;">音楽会会場としてご利用の場合</font></td>
            </tr>
            <tr>
              <td height="33" valign="middle"><font color="#FFFFFF" class="text4">午前 ： リハーサル<br>午後 ： コンサート</font></td>
              <td colspan="2" align="center" valign="middle"><font color="#333333" class="text4">400,000円</font></td>
              <td align="center" valign="middle"><img src="img/spacer.gif" width="1" height="1"></td>
            </tr>
            <tr>
            <td height="33" valign="middle"><p><font color="#FFFFFF" class="text4">午後 ： リハーサル<br>夜間 ： コンサート</font></td>
              <td align="center" valign="middle"><img src="img/spacer.gif" width="8" height="8"></td>
              <td colspan="2" align="center" valign="middle"><font color="#333333" class="text4">550,000円</font></td>
            </tr>
            <tr>
              <td height="32" valign="middle"><font color="#FFFFFF" class="text4">午前・午後： リハーサル<br>
                  夜間 ： コンサート</font></td>
              <td colspan="3" align="center" valign="middle"><font color="#333333" class="text4">650,000円</font></td>
            </tr>
          </table></td>
        </tr>
    </table>
    
      <table width="669" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
         <td><img src="img/spacer.gif" width="10" height="5"></td>
        </tr>
        <tr>
         <td align="right"><font color="#333333" class="text4">（金額は、すべて税抜表示です。）</font></td>
        </tr>
        <tr>
          <td><img src="img/spacer.gif" width="10" height="23"></td>
        </tr>
    </table>
      <table width="718" border="0" align="center" cellpadding="0" cellspacing="0" background="../img_cmn/bg_03.gif">
        <tr>
          <td><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
        </tr>
      </table>
      <table width="718" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#F3F3F3">
        <tr>
          <td width="10" valign="top"><img src="img/spacer.gif" width="1" height="1"></td>
          <td width="688" valign="top"><img src="img/spacer.gif" width="1" height="1"></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">平日と土・日・祝日は同一料金です。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">ホールを音楽会会場として使用の場合に限り、基本的な付帯設備（※）の使用料とその技術操作は無料とさせていただきます。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">ホールの使用料金には、案内業務（もぎり、場内案内、クロークの要員等）の人件費（基本時間内）を含みます。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">使用時間を超えてのホールのご使用は超過料金を頂戴いたします。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">ホールを音楽会以外の公演等に使用の場合は、上記のホールの使用料金のほかに、付帯設備費および必要経費を頂戴いたします。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">ホールを仕込み、ゲネプロ、リハーサル等に使用（ステージ上のみ）する場合の使用料金は、音楽会に限り各区分料金の50％とさせていただきます。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">使用時間にはステージ、楽屋、ホワイエ等での設営準備、搬入、搬出の時間も含まれております。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="img/annai_04.gif" width="10" height="11"></td>
          <td width="688" valign="top"><font color="#333333" class="text1">録音のみに使用される場合は、別途ご相談下さい。<br></font></td>
        </tr>
        <tr>
          <td width="10" valign="top"><img src="../img_cmn/spacer.gif" width="10" height="5"></td>
          <td width="688" align="right" valign="top"><a href="setubihi.html"><img src="img/annai_05.gif" alt="附帯設備費一覧" width="138" height="20" border="0"></a></td>
        </tr>
      </table>
      <table width="718" border="0" align="center" cellpadding="0" cellspacing="0" background="../img_cmn/bg_03.gif">
        <tr>
          <td><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
        </tr>
      </table>
      <table width="718" height="110" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="150" height="15"><img src="img/spacer.gif" width="10" height="15"></td>
          <td width="568" height="15" colspan="2"><img src="img/spacer.gif" width="10" height="15"></td>
        </tr>
        <tr>
          <td rowspan="3" valign="top"><font color="#194689" class="text1"><b>【お問い合わせ】</b></font></td>
          <td colspan="2" valign="top"><font color="#194689" class="text1"><b>第一生命ホール</b></font></td>
        </tr>
        <tr>
          <td width="90" valign="top"><font color="#194689" class="text2">〒１０４&#150;００５３</font></td>
          <td width="478" valign="top"><font color="#194689" class="text2">東京都中央区晴海１ー８ー９<br>
        晴海アイランド トリトンスクエア</font></td>
        </tr>
        <tr>
          <td colspan="2" valign="top"><font color="#194689" class="text2">ＴＥＬ：０３ー３５３２ー３５３５ ＦＡＸ：０３ー３５３２ー２７２３<br>
（受付時間：平日１０：００&#65374;１７：００）</font></td>
        </tr>
      </table>
      <table width="771" border="0" align="center" cellpadding="0" cellspacing="0" background="../img_cmn/bg_03.gif">
        <tr>
          <td><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
        </tr>
      </table>      
      <table width="771" border="0" align="center" cellpadding="0" cellspacing="0" background="../img_cmn/bg_03.gif">
        <tr>
          <td><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
        </tr>
      </table>      <table width="779" height="34" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　ご利用案内　|　<a href="../seatplan/index.html">座席表</a>　|　<a href="../schedule/index.php">コンサートスケジュール</a>　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
        </tr>
      </table>
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
        </tr>
        <tr>
          <td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
        </tr>
      </table></td>
    <td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
  </tr>
</table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
  </tr>
</table>
</center>
</body>
</html>