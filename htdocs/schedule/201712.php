<?php
//現在の月とMTから出力される月を比較するフラグをたてる
	$prevflag = 0;
	$nextflag = 0;
	$nowmonth = date('Ym');
	$forwardmonth = date('Ym', strtotime(date("Ym01")."+ 3 month"));

	if($forwardmonth <= 201712){
		$nextflag = 1;
	}elseif('201712' == '201801'){
		$prevflag = 1;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コンサートスケジュール</title>

<script language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><link rel="StyleSheet" type="text/css" href="../css/win.css" title="">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('../../img_cmn/navi01_2.gif','../../img_cmn/navi02_2.gif','../../img_cmn/navi03_2.gif','../../img_cmn/navi04_2.gif','../../img_cmn/navi06_2.gif')">

<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
<td width="779">
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
</tr>
<tr>
<td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" width="779" height="73" border="0"></a></td>
</tr>
</tbody></table>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><a href="../history/index.html" onmouseover="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
<td><a href="../about-hall/index.html" onmouseover="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
<td><a href="../annai/index.html" onmouseover="MM_swapImage('navi03','','../img_cmn/navi03_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi03_1.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></a></td>
<td><a href="../seatplan/index.html" onmouseover="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
<td><img src="../img_cmn/navi05_2.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></td>
<td><a href="../access/index.html" onmouseover="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
<td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
</tr>
</tbody></table>

<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="637" valign="top">
<table width="10" height="35" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="456" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="right"><img src="../img_cmn/schedule_01.gif" alt="コンサートスケジュール" width="456" height="21"></td>
</tr>
</tbody></table>
<table width="10" height="45" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="530" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="43"></td>
<td width="65"><img src="../img_cmn/schedule_02.gif" alt="水色枠" width="58" height="21"></td>
<td width="280"><font color="#003366" class="text1">→認定ＮＰＯ法人トリトン・アーツ・ネットワーク主催公演</font></td>
<td width="66"><img src="../img_cmn/schedule_03.gif" alt="灰色枠" width="59" height="21"></td>
<td width="76"><font color="#003366" class="text1">→共催公演</font></td>
</tr>
</tbody>
</table>



<table width="10" height="20" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
</td>
<td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
</tr>
</tbody></table>
<center>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="687">
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="163" align="center" valign="middle" background="../img_cmn/schedule_04.gif"><font color="#003366" class="text3">2017月12月</font></td>
<td width="418"></td>
<td width="106" height="33"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="38" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201505.php"><img src="../img_cmn/schedule_06.gif" width="38" height="21" border="0"></a><?php endif; ?></td>
<td width="302" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201505.php"><font color="#FFFFFF" class="text1">5月へ</font></a><?php endif; ?></td>
<td width="305" align="right" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201801.php"><font color="#FFFFFF" class="text1">1月へ</font></a><?php endif; ?></td>
<td width="42" align="right" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201801.php"><img src="../img_cmn/schedule_06_3.gif" width="42" height="21" border="0"></a><?php endif; ?></td>
</tr>
</tbody>
</table>



<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>

<td width="31" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201505.php"><img src="../img_cmn/schedule_07_1.gif" width="31" height="20" border="0"></a><?php endif;?></td>
<td width="309" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201505.php"><font color="#FFFFFF" class="text2">5月へ</font></a><?php endif;?></td>

<td width="315" align="right" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201801.php"><font color="#FFFFFF" class="text2">1月へ</font></a><?php endif;?></td>
<td width="32" align="right" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201801.php"><img src="../img_cmn/schedule_07_3.gif" width="32" height="20" border="0"></a><?php endif;?></td>

</tr>
</tbody></table>



<table width="687" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td height="30" align="center" valign="middle"></td>
</tr><tr>
<td height="55" align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　<a href="../annai/index.html">ご利用案内</a>　|　<a href="../seatplan/index.html">座席表</a>　|　コンサートスケジュール　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
</tr>
<tr>
<td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
</tr>
</tbody></table>
</td>
<td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
</tr>
</tbody></table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
</tr>
</tbody></table>
</center>
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>