<?php
//現在の月とMTから出力される月を比較するフラグをたてる
	$prevflag = 0;
	$nextflag = 0;
	$nowmonth = date('Ym');
	$forwardmonth = date('Ym', strtotime(date("Ym01")."+ 3 month"));

	if($forwardmonth <= 201806){
		$nextflag = 1;
	}elseif('201806' == '201803'){
		$prevflag = 1;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コンサートスケジュール</title>

<script language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><link rel="StyleSheet" type="text/css" href="../css/win.css" title="">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('../../img_cmn/navi01_2.gif','../../img_cmn/navi02_2.gif','../../img_cmn/navi03_2.gif','../../img_cmn/navi04_2.gif','../../img_cmn/navi06_2.gif')">

<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
<td width="779">
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
</tr>
<tr>
<td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" width="779" height="73" border="0"></a></td>
</tr>
</tbody></table>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><a href="../history/index.html" onmouseover="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
<td><a href="../about-hall/index.html" onmouseover="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
<td><a href="../annai/index.html" onmouseover="MM_swapImage('navi03','','../img_cmn/navi03_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi03_1.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></a></td>
<td><a href="../seatplan/index.html" onmouseover="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
<td><img src="../img_cmn/navi05_2.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></td>
<td><a href="../access/index.html" onmouseover="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
<td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
</tr>
</tbody></table>

<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="637" valign="top">
<table width="10" height="35" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="456" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="right"><img src="../img_cmn/schedule_01.gif" alt="コンサートスケジュール" width="456" height="21"></td>
</tr>
</tbody></table>
<table width="10" height="45" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="530" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="43"></td>
<td width="65"><img src="../img_cmn/schedule_02.gif" alt="水色枠" width="58" height="21"></td>
<td width="280"><font color="#003366" class="text1">→認定ＮＰＯ法人トリトン・アーツ・ネットワーク主催公演</font></td>
<td width="66"><img src="../img_cmn/schedule_03.gif" alt="灰色枠" width="59" height="21"></td>
<td width="76"><font color="#003366" class="text1">→共催公演</font></td>
</tr>
</tbody>
</table>



<table width="10" height="20" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
</td>
<td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
</tr>
</tbody></table>
<center>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="687">
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="163" align="center" valign="middle" background="../img_cmn/schedule_04.gif"><font color="#003366" class="text3">2018月6月</font></td>
<td width="418"></td>
<td width="106" height="33"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="38" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201805.php"><img src="../img_cmn/schedule_06.gif" width="38" height="21" border="0"></a><?php endif; ?></td>
<td width="302" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201805.php"><font color="#FFFFFF" class="text1">5月へ</font></a><?php endif; ?></td>
<td width="305" align="right" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><?php endif; ?></td>
<td width="42" align="right" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><?php endif; ?></td>
</tr>
</tbody>
</table>



<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180603175010">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月 3日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>東京アカデミーオーケストラ　第５３回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆東京アカデミーオーケストラ</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ラヴェル：組曲「クープランの墓」<br />
◆プーランク：シンフォニエッタ FP.141<br />
◆シューベルト：交響曲第5番 変ロ長調 D.485</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，５００（全自由）<br />
※シニア（６０歳以上）・学生限定【当日券無料サービス】<br />
当日券の発売がある場合、公演当日時点で満６０歳以上の方ならびに在学中の学生生徒のかたは、ご本人の年齢が分かる身分証明書（パスポート、運転免許証、保険証など）または学生証をご提示いただいた場合、当日券を無料でご購入いただけます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年３月３日（土）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://tao.jpn.org/" target="_blank">東京アカデミーオーケストラ</a>（夏目） ０９０-７２９１-６７５４<a href="http://tao.jpn.org" target="_blank"><br /></a></font>
</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1">カンフェティ ０１２０-２４０-５４０</font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180609165650">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#E9E9E9">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月 9日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#E9E9E9">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>バズ・ファイブ コンサート What's the Buzz? ２０１８</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆バズ・ファイブ（金管五重奏団）<br />［上田じん／松山 萌（トランペット）友田雅美（ホルン）加藤直明（トロンボーン）石丸薫恵（テューバ）］</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆曲目未定</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆¥３，０００（一般自由）<br />
◆¥２，０００（高校生以下自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年３月上旬</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180610152506">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月10日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p><span style="color: #000000;" class="text1" color="#000000">アンサンブル・フラン 第４１回定期演奏会</span></p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1"><span class="text1">◆</span></span>山口裕之（ゲストコンサートマスター）<br />◆アンサンブル・フラン（弦楽合奏）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ベートーヴェン：弦楽四重奏曲第15番 イ短調 Op.132（弦楽合奏版）<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：未定</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1">アンサンブル･フラン事務局（谷） ０３-５９４５-２２３５</span></font>
</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1">e+（イープラス）にて発売予定</font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180615194631">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月15日(金)</a><br></b></font>
<font class="text1"><br>19：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>小菅優＆石坂団十郎<br />ベートーヴェン：チェロ・ソナタ全曲演奏会Ⅰ</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆小菅優（ピアノ）<br />◆石坂団十郎（チェロ）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>〈オール・ベートーヴェン・プログラム〉<br />
◆《マカベウスのユダ》の主題による12の変奏曲 ト長調 WoO45<br />
◆チェロ・ソナタ第2番 ト短調 Op.5-2<br />
◆チェロ・ソナタ第1番 ヘ長調 Op.5-1<br />
◆《魔笛》より「娘か女房か」の主題による12の変奏曲 ヘ長調 Op.66<br />
◆チェロ・ソナタ第4番 ハ長調 Op.102-1</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥９，０００（２公演セット券Ｓ指定）<br />
◆￥５，０００（Ｓ指定）<br />
◆￥４，５００（Ａ指定）<br />
◆￥３，５００（Ｂ指定）<br />
◆￥１，５００（ヤング指定：小学生以上、２５歳以下）<br />
＊２公演セット券は、１２月１５日（土）第２回公演とのセット<br />
</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月２７日（火）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1"><a href="http://pia.jp/" target="_blank">チケットぴあ</a>〔Pコード：104-201〕＊S・A・B席のみ取扱い</font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180616175329">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月16日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>Brilliant Harmony　第２９回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆松下　耕（指揮）<br />◆前田勝則（ピアノ）<br />◆Brilliant Harmony（合唱）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>＜メインステージ＞<br />
◆松下耕／古川薫作詞：赤トンボの翼<br />
＜民謡ステージ＞<br />
◆松下耕：謡舞／Warabe-Uta／俵積み唄<br />
＜若い作曲家シリーズ＞<br />
◆松下耕／征矢泰子作詞：治癒</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，０００（前売一般自由）<br />
◆￥２，０００（前売学生自由）<br />
◆￥３，５００（当日自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年３月下旬予定</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://www.brilliantharmony.com/" target="_blank">Brilliant Harmony</a>演奏会事務局　０９０-３６９８-５２７４</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180617111311">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月17日(日)</a><br></b></font>
<font class="text1"><br>15：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>NS-４ クワトロ 第１９回演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆伊集院俊光／岸信介／岡本俊久／金川明裕（指揮）</div>
<div>◆茅ヶ崎フラウエンコール／コール・サルビア／合唱団たちばな／<br />ヘンデル室内合唱団（合唱）</div>
<div></div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆福島雄次郎：眠れ眠れ童／朝の祈り<br />
◆信長貴富：夜明けから日暮まで／きみうたえよ<br />
◆大竹くみ：聖母マリアのアンティフォナ<br />
◆ヘンリー・パーセル：メアリー女王葬送の音楽<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1"><p>◆チケット発売日：２０１８年４月１日（日）予定</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p>NS-４クワトロ（岡本） ０４２-３９１-１０３５</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180624153550">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月24日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p><span class="text1" style="color: #000000;" color="#000000">弥生室内管弦楽団 第４９回演奏会</span></p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1">◆小出英樹（指揮）<br />◆弥生室内管弦楽団（管弦楽）</span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ロッシーニ：歌劇「チェネレントラ」序曲<br />
◆モーツァルト：交響曲第40番 ト短調 K.550<br />
◆シベリウス：交響曲第5番 変ホ長調 Op.82</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年４月１日（日）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://www2.biglobe.ne.jp/~yayoi/" target="_blank">弥生室内管弦楽団</a>（内藤） ０９０-８１１４-３２６２</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180630152506">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">6月30日(土)</a><br></b></font>
<font class="text1"><br>18：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p><span class="text1" style="color: #000000;" color="#000000">ARTE TOKYO　第８回定期公演</span></p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1"><span class="text1">◆井上泰信（指揮）</span></span><br />◆伊藤亜美（ヴァイオリン）<br />◆Irina Kolosova（マンドリン）<br />◆ARTE TOKYO（マンドリンオーケストラ）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆メンデルスゾーン：劇音楽「真夏の夜の夢」より<br />
◆藤掛廣幸：パストラルファンタジー（ヴァイオリン独奏付き）<br />
◆ポドガイツ：マンドリン協奏曲<br />
◆末廣健児：委嘱作品</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（前売一般自由）<br />
◆￥１，５００（前売学生自由）<br />
◆￥１，０００（前売高校生以下自由）<br />
※当日は各￥５００増</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年４月中旬</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1"><a href="http://www.arte-mandolin.com/" target="_blank">ARTE TOKYO</a>事務局 ０９０-１０７６-２５８９</span></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>

<td width="31" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201805.php"><img src="../img_cmn/schedule_07_1.gif" width="31" height="20" border="0"></a><?php endif;?></td>
<td width="309" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201805.php"><font color="#FFFFFF" class="text2">5月へ</font></a><?php endif;?></td>

<td width="315" align="right" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><?php endif;?></td>
<td width="32" align="right" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><?php endif;?></td>

</tr>
</tbody></table>



<table width="687" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td height="30" align="center" valign="middle"></td>
</tr><tr>
<td height="55" align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　<a href="../annai/index.html">ご利用案内</a>　|　<a href="../seatplan/index.html">座席表</a>　|　コンサートスケジュール　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
</tr>
<tr>
<td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
</tr>
</tbody></table>
</td>
<td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
</tr>
</tbody></table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
</tr>
</tbody></table>
</center>
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>