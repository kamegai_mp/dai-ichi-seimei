<?php
//現在の月とMTから出力される月を比較するフラグをたてる
	$prevflag = 0;
	$nextflag = 0;
	$nowmonth = date('Ym');
	$forwardmonth = date('Ym', strtotime(date("Ym01")."+ 3 month"));

	if($forwardmonth <= 201803){
		$nextflag = 1;
	}elseif('201803' == '201803'){
		$prevflag = 1;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コンサートスケジュール</title>

<script language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><link rel="StyleSheet" type="text/css" href="../css/win.css" title="">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('../../img_cmn/navi01_2.gif','../../img_cmn/navi02_2.gif','../../img_cmn/navi03_2.gif','../../img_cmn/navi04_2.gif','../../img_cmn/navi06_2.gif')">

<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
<td width="779">
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
</tr>
<tr>
<td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" width="779" height="73" border="0"></a></td>
</tr>
</tbody></table>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><a href="../history/index.html" onmouseover="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
<td><a href="../about-hall/index.html" onmouseover="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
<td><a href="../annai/index.html" onmouseover="MM_swapImage('navi03','','../img_cmn/navi03_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi03_1.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></a></td>
<td><a href="../seatplan/index.html" onmouseover="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
<td><img src="../img_cmn/navi05_2.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></td>
<td><a href="../access/index.html" onmouseover="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
<td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
</tr>
</tbody></table>

<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="637" valign="top">
<table width="10" height="35" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="456" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="right"><img src="../img_cmn/schedule_01.gif" alt="コンサートスケジュール" width="456" height="21"></td>
</tr>
</tbody></table>
<table width="10" height="45" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="530" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="43"></td>
<td width="65"><img src="../img_cmn/schedule_02.gif" alt="水色枠" width="58" height="21"></td>
<td width="280"><font color="#003366" class="text1">→認定ＮＰＯ法人トリトン・アーツ・ネットワーク主催公演</font></td>
<td width="66"><img src="../img_cmn/schedule_03.gif" alt="灰色枠" width="59" height="21"></td>
<td width="76"><font color="#003366" class="text1">→共催公演</font></td>
</tr>
</tbody>
</table>



<table width="10" height="20" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
</td>
<td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
</tr>
</tbody></table>
<center>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="687">
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="163" align="center" valign="middle" background="../img_cmn/schedule_04.gif"><font color="#003366" class="text3">2018月3月</font></td>
<td width="418"></td>
<td width="106" height="33"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="38" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201503.php"><img src="../img_cmn/schedule_06.gif" width="38" height="21" border="0"></a><?php endif; ?></td>
<td width="302" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201503.php"><font color="#FFFFFF" class="text1">3月へ</font></a><?php endif; ?></td>
<td width="305" align="right" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201804.php"><font color="#FFFFFF" class="text1">4月へ</font></a><?php endif; ?></td>
<td width="42" align="right" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201804.php"><img src="../img_cmn/schedule_06_3.gif" width="42" height="21" border="0"></a><?php endif; ?></td>
</tr>
</tbody>
</table>
<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180317151150">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月17日(土)</a><br></b></font>
<font class="text1"><br>14：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>ＴＯＫＹＯ ＦＭ 少年合唱団　第３４回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆佐藤 宏／須藤桂司（指揮）</div>
<div>◆田村ルリ／小林茉莉花（ピアノ）<br />◆ＴＯＫＹＯ ＦＭ 少年合唱団（合唱）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ぼくらのレパートリー～ＴＯＫＹＯ ＦＭ 少年合唱団愛唱歌～<br />
◆ハイドン：聖レオポルトのミサMH837 より<br />
◆三善晃／谷川俊太郎作詞：わらべうた</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，５００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年１月中旬</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://www.tfm.co.jp/boys-chorus/" target="_blank">ＴＯＫＹＯ ＦＭ 少年合唱団 </a>事務局 ０３-３２２１-００８０</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180318180206">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月18日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>室内楽ホールdeオペラ～林美智子の『フィガロ』！</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆林美智子（メゾソプラノ、日本語台詞台本・構成・演出）<br />◆澤畑恵美／鵜木絵里（ソプラノ）<br />◆竹本節子（メゾソプラノ）<br />◆望月哲也（テノール）<br />◆加耒徹／黒田博／晴雅彦（バリトン）<br />◆池田直樹（バス・バリトン）<br />◆河原忠之（ピアノ）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆モーツァルト：歌劇「フィガロの結婚」より<br />
“重唱のみで構成されたモーツァルト「フィガロの結婚」ダイジェスト版”</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥６，０００（Ｓ指定）<br />
◆￥５，０００（Ａ指定）<br />
◆￥２，０００（Ｂ指定）<br />
◆￥１，５００（ヤング指定：小学生以上、２５歳以下）　<br />
※Ｂ席は舞台が非常にご覧になりにくい２階席サイドです。<br />
予めご了承の上、お買い求めください。</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1"><a href="http://pia.jp/%20" target="_blank">チケットぴあ</a> 〔Pコード：３４０-３７２〕＊S席のみ取扱い<a href="http://pia.jp/" target="_blank"><br /></a></font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180321180206">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月21日(水)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>室内楽ホールdeオペラ～林美智子の『フィガロ』！</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆林美智子（メゾソプラノ、日本語台詞台本・構成・演出）<br />◆澤畑恵美／鵜木絵里（ソプラノ）<br />◆竹本節子（メゾソプラノ）<br />◆望月哲也（テノール）<br />◆加耒徹／黒田博／晴雅彦（バリトン）<br />◆池田直樹（バス・バリトン）<br />◆河原忠之（ピアノ）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆モーツァルト：歌劇「フィガロの結婚」より<br />
“重唱のみで構成されたモーツァルト「フィガロの結婚」ダイジェスト版”</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥６，０００（Ｓ指定）<br />
◆￥５，０００（Ａ指定）<br />
◆￥２，０００（Ｂ指定）<br />
◆￥１，５００（ヤング指定：小学生以上、２５歳以下）　<br />
＊Ｂ席は舞台が非常にご覧になりにくい２階席サイドです。<br />
予めご了承の上、お買い求めください。</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1"><a href="http://pia.jp/%20" target="_blank">チケットぴあ</a> 〔Pコード：３４０-３７２〕 ＊S席のみ取扱い<a href="http://pia.jp/" target="_blank"><br /></a></font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180324150704">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月24日(土)</a><br></b></font>
<font class="text1"><br>14：20 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>ローランド・ピアノ・ミュージックフェスティバル２０１７ ファイナル</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆ローランド・ピアノ・ミュージックフェスティバル2017本選代表者<br />（小学生・中高生・一般 計37名）</div>
<div>◆栗田博文（指揮）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆曲目未定<br />
ローランド デジタル・グランドピアノと、ストリングス／ベース／木管楽器／シンセサイザー／ローランドVドラムの編成によるミニオーケストラのアンサンブル演奏<br />
＊演奏曲、曲順は当日発表</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，９５０（一般自由）<br />
◆￥１，０８０（ローランド・ミュージック・スクール会員（生徒・講師・講習生））</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1"><div>◆チケット発売日：当日のみ</div>
<div></div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><div><a href="http://www.roland.co.jp/school/contest/pmf/" target="_blank">ローランド（株）ピアノ・ミュージックフェスティバル事務局<br /><span style="color: #000000; line-height: 1.62;">０３-６６８４-３４８９</span></a></div></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180325130000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月25日(日)</a><br></b></font>
<font class="text1"><br>13：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>第４１回ピティナ・ピアノコンペティション　入賞者記念コンサート【第１部】</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆<span>第４１回ピティナ・ピアノコンペティション ソロ・デュオ部門全国決勝大会 入賞者</span>（ピアノ）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ブラームス：ピアノ・ソナタ第3番ヘ短調op.5より 第5楽章<br />
◆シューマン／リスト編：献呈S.566<br />
◆ドビュッシー：アラベスク第1番<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，５００（一般指定：各部）<br />
◆￥３，０００（学生・ピティナ会員指定：各部）<br />
◆￥５００（２０１７年度参加者指定：各部）<br />
＊詳細は主催者ＨＰをご覧ください</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://www.piano.or.jp/concert/yp/solo-duo/" target="_blank">ピティナ（一般社団法人全日本ピアノ指導者協会）<br /></a>０３-３９４４-１５８３（平日10:00～18:00）</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180325150923">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月25日(日)</a><br></b></font>
<font class="text1"><br>16：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>第４１回ピティナ・ピアノコンペティション　入賞者記念コンサート【第２部】</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>
<div>◆<span>第４１回ピティナ・ピアノコンペティション ソロ・デュオ部門全国決勝大会 入賞者</span>（ピアノ）</div>
</div>
<div></div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ショパン：ポロネーズ第1番嬰ハ短調op.26-1<br />
◆ラヴェル：「夜のガスパール」より スカルボ<br />
◆プーランク：ピアノ、オーボエとファゴットのための三重奏曲より 第1、3楽章<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，５００（一般指定：各部）<br />
◆￥３，０００（学生・ピティナ会員指定：各部）<br />
◆￥５００（２０１７年度参加者指定：各部）<br />
＊詳細は主催者ＨＰをご覧ください</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://www.piano.or.jp/concert/yp/solo-duo/" target="_blank">ピティナ（一般社団法人全日本ピアノ指導者協会）<br /></a>０３-３９４４-１５８３（平日10:00～18:00）</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180327133000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月27日(火)</a><br></b></font>
<font class="text1"><br>12：15 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>第一生命ホール　ロビーコンサート<br />～室内楽アウトリーチセミナー講師と受講生による～</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆松原勝也（ヴァイオリン）<br />◆セミナー受講生：今高友香（ヴァイオリン）、岩下恵美（ヴィオラ）、福原明音（チェロ）<br />◆ゲスト：柳瀬省太（ヴィオラ）、松本亜優（チェロ）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆チャイコフスキー：フィレンツェの思い出op.70　他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆入場無料（全自由）<br />
＊未就学児入場不可<br />
＊来場者多数の場合は、入場をお断りさせて頂く場合がございます。</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180331150704">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">3月31日(土)</a><br></b></font>
<font class="text1"><br>13：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>第５回全日本マンドリン合奏コンクール</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆静岡東高校マンドリン部／静岡市立高校マンドリン部／聖セシリア女子中高マンドリンクラブ／アンサンブル・ビアンカフィオーリ／SSC Orchestra（マンドリンオーケストラ）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆コンクール課題曲および自由曲</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆入場無料（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1"><span style="color: #ff0000;">※開場・開演時間を変更させていただきました。</span><br /><span style="color: #ff0000;">変更前　１２：３０開場／１３：００開演</span><br /><span style="color: #ff0000;">変更後　１３：００開場／１３：３０開演</span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://www.arte-mandolin.com/" target="_blank">NPO法人ARTE MANDOLINISTICA</a>コンクール事務局<br />０９０-１０７６-２５８９</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>

<td width="31" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201503.php"><img src="../img_cmn/schedule_07_1.gif" width="31" height="20" border="0"></a><?php endif;?></td>
<td width="309" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201503.php"><font color="#FFFFFF" class="text2">3月へ</font></a><?php endif;?></td>

<td width="315" align="right" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201804.php"><font color="#FFFFFF" class="text2">4月へ</font></a><?php endif;?></td>
<td width="32" align="right" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201804.php"><img src="../img_cmn/schedule_07_3.gif" width="32" height="20" border="0"></a><?php endif;?></td>

</tr>
</tbody></table>



<table width="687" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td height="30" align="center" valign="middle"></td>
</tr><tr>
<td height="55" align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　<a href="../annai/index.html">ご利用案内</a>　|　<a href="../seatplan/index.html">座席表</a>　|　コンサートスケジュール　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
</tr>
<tr>
<td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
</tr>
</tbody></table>
</td>
<td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
</tr>
</tbody></table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
</tr>
</tbody></table>
</center>
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>