<?php
//現在の月とMTから出力される月を比較するフラグをたてる
	$prevflag = 0;
	$nextflag = 0;
	$nowmonth = date('Ym');
	$forwardmonth = date('Ym', strtotime(date("Ym01")."+ 3 month"));

	if($forwardmonth <= 201805){
		$nextflag = 1;
	}elseif('201805' == '201803'){
		$prevflag = 1;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コンサートスケジュール</title>

<script language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><link rel="StyleSheet" type="text/css" href="../css/win.css" title="">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('../../img_cmn/navi01_2.gif','../../img_cmn/navi02_2.gif','../../img_cmn/navi03_2.gif','../../img_cmn/navi04_2.gif','../../img_cmn/navi06_2.gif')">

<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
<td width="779">
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
</tr>
<tr>
<td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" width="779" height="73" border="0"></a></td>
</tr>
</tbody></table>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><a href="../history/index.html" onmouseover="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
<td><a href="../about-hall/index.html" onmouseover="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
<td><a href="../annai/index.html" onmouseover="MM_swapImage('navi03','','../img_cmn/navi03_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi03_1.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></a></td>
<td><a href="../seatplan/index.html" onmouseover="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
<td><img src="../img_cmn/navi05_2.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></td>
<td><a href="../access/index.html" onmouseover="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
<td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
</tr>
</tbody></table>

<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="637" valign="top">
<table width="10" height="35" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="456" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="right"><img src="../img_cmn/schedule_01.gif" alt="コンサートスケジュール" width="456" height="21"></td>
</tr>
</tbody></table>
<table width="10" height="45" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="530" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="43"></td>
<td width="65"><img src="../img_cmn/schedule_02.gif" alt="水色枠" width="58" height="21"></td>
<td width="280"><font color="#003366" class="text1">→認定ＮＰＯ法人トリトン・アーツ・ネットワーク主催公演</font></td>
<td width="66"><img src="../img_cmn/schedule_03.gif" alt="灰色枠" width="59" height="21"></td>
<td width="76"><font color="#003366" class="text1">→共催公演</font></td>
</tr>
</tbody>
</table>



<table width="10" height="20" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
</td>
<td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
</tr>
</tbody></table>
<center>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="687">
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="163" align="center" valign="middle" background="../img_cmn/schedule_04.gif"><font color="#003366" class="text3">2018月5月</font></td>
<td width="418"></td>
<td width="106" height="33"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="38" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201804.php"><img src="../img_cmn/schedule_06.gif" width="38" height="21" border="0"></a><?php endif; ?></td>
<td width="302" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201804.php"><font color="#FFFFFF" class="text1">4月へ</font></a><?php endif; ?></td>
<td width="305" align="right" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201806.php"><font color="#FFFFFF" class="text1">6月へ</font></a><?php endif; ?></td>
<td width="42" align="right" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201806.php"><img src="../img_cmn/schedule_06_3.gif" width="42" height="21" border="0"></a><?php endif; ?></td>
</tr>
</tbody>
</table>



<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180503171757">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月 3日(木)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>西六郷少年少女合唱団２０１８定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1">◆小池直樹（指揮）<br />◆中島　剛（ピアノ）<br />◆西六郷少年少女合唱団／西六郷小学校合唱部（合唱）<br /></span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆南　安雄：こどものための旅のうた「緑のキップ」<br />
◆間宮芳生：烏かねもん勘三郎<br />
◆小倉　朗：ほたるこい<br />
◆シューベルト：きけ、ひばり<br />
◆シューマン：流浪の民<br />
◆ワルトトイフェル：スケートをする人々<br />
◆富澤　裕：エスペランサ<br />
◆横山裕美子：シーラカンスをとりにいこう<br />
◆第８５回ＮＨＫ全国学校音楽コンクール課題曲より<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全指定）</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://www.nishiroku.com/" target="_blank">西六郷少年少女合唱団</a>（小池） ０３-３７５９-６６１２（夜間のみ）</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180505153550">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月 5日(土)</a><br></b></font>
<font class="text1"><br>18：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p><span class="text1" style="color: #000000;" color="#000000">Orchestra X　演奏会</span></p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1">◆直井大輔（指揮）<br />◆實川　風（ピアノ）<br />◆<span class="text1" style="color: #000000;" color="#000000">Orchestra X</span>（管弦楽）</span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ベートーヴェン：ピアノ協奏曲第5番「皇帝」変ホ長調Op.73<br />
◆ブラームス：交響曲第1番ハ短調Op.68</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥５００（全指定）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年３月予定</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1" style="color: #000000;" color="#000000"><a href="http://10orch10.starfree.jp/" target="_blank">Orchestra X</a> 　<a href="mailto:10orch10@gmail.com">10orch10@gmail.com</a></span></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180512111803">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月12日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>東京六甲男声合唱団 第７回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆竹内公一／静川靖敏／竹本鉄三（指揮）<br />◆佐藤良子／石渡真知子（ピアノ）<br />◆東京六甲男声合唱団（合唱）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆シューベルト：歌曲集「冬の旅」op.89より　おやすみ／菩堤樹<br />
◆大中　恩：作品集より　ふるみち／草原の別れ<br />
◆ミュージカルと映画の名曲集：魅惑の宵／「キャッツ」より　メモリー<br />
◆信長貴富：男声合唱曲より　くちびるに歌を／浅春偶語<br />
他<br />
</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，５００（全自由）<br />
◆￥５００（学生自由）</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p>神戸大学東京六甲クラブ事務局　０３-３２１１-２９１６</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180513152506">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月13日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>Ｋ-ｍｉｏ ｃｈｏｒ　第５回演奏会</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1"><span class="text1">◆神尾　昇（指揮）<br />◆</span></span>薮田瑞穂（ソプラノ）<br />◆青地英幸（テノール）　<br />◆川原彩子（ピアノ）<br />◆８jo室内管弦楽団（管弦楽）<br /><span class="text1">◆Ｋ-ｍｉｏ ｃｈｏｒ（合唱）</span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆萩原英彦／矢沢宰作詞：混声合唱組曲「光る砂漠」<br />
◆レフィーチェ：スターバト・マーテル<br />
◆ヘンデル：「メサイア」抜粋</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月１日（木）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1"><a href="https://k-mio-chor.jimdo.com/" target="_blank">Ｋ-ｍｉｏ ｃｈｏｒ</a>（神尾） ０９０-８４９１-０２９４</span></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180515110908">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月15日(火)</a><br></b></font>
<font class="text1"><br>11：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p><span style="line-height: 1.62;">雄大と行く昼の音楽さんぽ　第１３回　<br />鈴木舞　ヴァイオリン、光が薫るとき<br /></span></p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆鈴木舞（ヴァイオリン）<br />◆實川風（ピアノ）<br />◆山野雄大（ご案内）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ドビュッシー（ハイフェッツ編曲）：「子供の領分」より　ゴリウォーグのケークウォーク<br />
◆マスネ：タイスの瞑想曲<br />
◆リリー・ブーランジェ：ノクターン／コルテージュ（行列） <br />
◆サティ：ジュ・トゥ・ヴ（あなたが欲しい）<br />
◆ラヴェル：ツィガーヌ<br />
◆プーランク：ヴァイオリン・ソナタ<br />
◆イザイ：サン=サーンスの「ワルツ形式の練習曲」によるカプリース</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，０００（２公演セット券指定）<br />
◆￥２，０００（一般指定）<br />
◆￥１，５００（お友だち割指定：同一公演３枚以上同時購入で１枚あたり）<br />
※２公演セット券は、第１４回７月４日（水）藤木大地（カウンターテナー）＆福田進一（ギター）とのセット</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月２７日（火）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1"><a href="http://t.pia.jp/" target="_blank">チケットぴあ</a>〔Pコード：１０４-１９４〕＊一般券のみ取扱い</font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180519111500">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月19日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>ザ・ファインアーツ・フィルハーモニック　第１４回演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆田中一嘉（指揮）<br />◆宮川正雪（ヴァイオリン）</div>
<div>◆ザ・ファインアーツ・フィルハーモニック（管弦楽）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆モーツァルト：交響曲第32番ト長調K.318<br />
◆ブラームス：ヴァイオリン協奏曲二長調op.77／交響曲第4番ホ短調op.98</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月１日（木）予定</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><div><a href="http://music.geocities.jp/fineartsphil/" target="_blank">ザ・ファインアーツ・フィルハーモニック</a>（潮見） ０９０-１４６９-７２５５</div></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180520111613">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月20日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>中央区交響楽団　第２４回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆佐藤雄一（指揮）<br />◆中央区交響楽団（管弦楽）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ウォルトン：「スピットファイア」より　前奏曲とフーガ <br />
◆ブリテン：青少年のための管弦楽入門op.34 <br />
◆メンデルスゾーン：交響曲第3番イ短調「スコットランド」op.56</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，０００（全自由）<br />
※中学生以下無料、未就学児入場可<br />
※当日券のみ<br />
</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://www.cso-arts.net/" target="_blank">中央区交響楽団事務局</a> ０３-５５６０-２０９０</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180526110641">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月26日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>合唱団「郁（ふみ）の会」 第１７回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆下村郁哉（指揮）<br />◆菅又美玲／莟道子（ソプラノ）<br />◆田辺いづみ（アルト）<br />◆佐藤敦史（テノール）<br />◆三浦克次（バリトン）<br />◆合唱団「郁（ふみ）の会」（合唱）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆モーツァルト：戴冠ミサ<br />
◆グノー／シューベルト：アヴェ・マリア<br />
◆ヘンデル：「メサイア」より　アーメンコーラス<br />
◆アメリカン　ポップス<br />
◆落語：演目「ベートーヴェンの第九」<br />
他<br />
</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月中旬</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="https://choir-fuminokai.jimdo.com/" target="_blank">合唱団「郁（ふみ）の会」</a><span>（下村） </span>０３-５６７８-５２６７</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180527111803">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月27日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>モーツァルト・アンサンブル・オーケストラ 第３４回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆藤原義章（指揮）</div>
<div>◆<a href="http://orchestra.musicinfo.co.jp/~MEO1984/" target="_blank">モーツァルト・アンサンブル・オーケストラ</a>（管弦楽）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆J.C.バッハ：シンフォニア変ロ長調op.18-2<br />
◆モーツァルト：交響曲第31番ニ長調「パリ」K.297<br />
◆シベリウス：組曲「ペレアスとメリザンド」op.46<br />
◆ハイドン：交響曲第103番変ホ長調Hob.I:103</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月１日（木）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p>株式会社新演 ０３-３５６１-５０１２</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180530090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月30日(水)</a><br></b></font>
<font class="text1"><br>10：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180530090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月30日(水)</a><br></b></font>
<font class="text1"><br>11：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180530090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月30日(水)</a><br></b></font>
<font class="text1"><br>13：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180531090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月31日(木)</a><br></b></font>
<font class="text1"><br>10：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180531090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月31日(木)</a><br></b></font>
<font class="text1"><br>11：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180531090000">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#DCFBFB">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">5月31日(木)</a><br></b></font>
<font class="text1"><br>13：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#DCFBFB">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>子どもといっしょにクラシック<br />～第２５回ロビーでよちよちコンサート</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆内藤歌子（ヴァイオリン）<br />◆小澤　剛（コントラバス）<br />◆北岡羽衣（クラリネット）<br />◆永井綾子（トランペット）<br />◆富田真以子（打楽器）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆6ヶ月～3歳の乳幼児のお子さまとその保護者の方向けの、第一生命ホールの「ロビー」で行う約45分間の小さなコンサート。<br />
開放感ある第一生命ホールのロビーで輪になって、音楽を聴くだけでなく、音楽と一緒に体を動かしたり手を叩いたり、親子揃って体中で音楽を楽しめます。</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆親子１組￥１，０００<br />
トリトンアーツホームページ（http://www.triton-arts.net）の「ロビーでよちよち申込フォーム」よりお申し込みください。<br />
先着順で受付。定員になり次第締め切り。<br />
２０１８年５月８日（火）午前１１：００から先着順</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<a href="http://www.triton-arts.net" target="_blank"><font class="text1">トリトンアーツ・チケットデスク ０３-３５３２-５７０２</font></a>

</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>

<td width="31" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201804.php"><img src="../img_cmn/schedule_07_1.gif" width="31" height="20" border="0"></a><?php endif;?></td>
<td width="309" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201804.php"><font color="#FFFFFF" class="text2">4月へ</font></a><?php endif;?></td>

<td width="315" align="right" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201806.php"><font color="#FFFFFF" class="text2">6月へ</font></a><?php endif;?></td>
<td width="32" align="right" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201806.php"><img src="../img_cmn/schedule_07_3.gif" width="32" height="20" border="0"></a><?php endif;?></td>

</tr>
</tbody></table>



<table width="687" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td height="30" align="center" valign="middle"></td>
</tr><tr>
<td height="55" align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　<a href="../annai/index.html">ご利用案内</a>　|　<a href="../seatplan/index.html">座席表</a>　|　コンサートスケジュール　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
</tr>
<tr>
<td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
</tr>
</tbody></table>
</td>
<td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
</tr>
</tbody></table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
</tr>
</tbody></table>
</center>
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>