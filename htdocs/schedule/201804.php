<?php
//現在の月とMTから出力される月を比較するフラグをたてる
	$prevflag = 0;
	$nextflag = 0;
	$nowmonth = date('Ym');
	$forwardmonth = date('Ym', strtotime(date("Ym01")."+ 3 month"));

	if($forwardmonth <= 201804){
		$nextflag = 1;
	}elseif('201804' == '201803'){
		$prevflag = 1;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コンサートスケジュール</title>

<script language="javascript">
<!--
if(navigator.appVersion.indexOf("Mac",0) != -1){
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/mac.css" title="">');
}
else {
        document.write('<LINK rel="StyleSheet" type="text/css" href="../css/win.css" title="">');
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><link rel="StyleSheet" type="text/css" href="../css/win.css" title="">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('../../img_cmn/navi01_2.gif','../../img_cmn/navi02_2.gif','../../img_cmn/navi03_2.gif','../../img_cmn/navi04_2.gif','../../img_cmn/navi06_2.gif')">

<center>
<table width="855" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td width="38" background="../img_cmn/bg01.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
<td width="779">
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="9"></td>
</tr>
<tr>
<td><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td bgcolor="#01307E"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td><a href="../index.html"><img src="../img_cmn/head_01.jpg" alt="第一生命ホール" width="779" height="73" border="0"></a></td>
</tr>
</tbody></table>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><a href="../history/index.html" onmouseover="MM_swapImage('navi01','','../img_cmn/navi01_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi01_1.gif" alt="ホールの歴史" name="navi01" width="107" height="22" border="0" id="navi01"></a></td>
<td><a href="../about-hall/index.html" onmouseover="MM_swapImage('navi02','','../img_cmn/navi02_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi02_1.gif" alt="施設概要" name="navi02" width="106" height="22" border="0" id="navi02"></a></td>
<td><a href="../annai/index.html" onmouseover="MM_swapImage('navi03','','../img_cmn/navi03_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi03_1.gif" alt="ご利用案内" name="navi03" width="106" height="22" border="0" id="navi03"></a></td>
<td><a href="../seatplan/index.html" onmouseover="MM_swapImage('navi04','','../img_cmn/navi04_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi04_1.gif" alt="座席表" name="navi04" width="106" height="22" border="0" id="navi04"></a></td>
<td><img src="../img_cmn/navi05_2.gif" alt="コンサートスケジュール" name="navi05" width="106" height="22" border="0" id="navi05"></td>
<td><a href="../access/index.html" onmouseover="MM_swapImage('navi06','','../img_cmn/navi06_2.gif',1)" onmouseout="MM_swapImgRestore()"><img src="../img_cmn/navi06_1.gif" alt="アクセスガイド" name="navi06" width="106" height="22" border="0" id="navi06"></a></td>
<td><img src="../img_cmn/head_02.gif" width="142" height="22"></td>
</tr>
</tbody></table>

<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="637" valign="top">
<table width="10" height="35" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="456" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="right"><img src="../img_cmn/schedule_01.gif" alt="コンサートスケジュール" width="456" height="21"></td>
</tr>
</tbody></table>
<table width="10" height="45" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
<table width="530" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="43"></td>
<td width="65"><img src="../img_cmn/schedule_02.gif" alt="水色枠" width="58" height="21"></td>
<td width="280"><font color="#003366" class="text1">→認定ＮＰＯ法人トリトン・アーツ・ネットワーク主催公演</font></td>
<td width="66"><img src="../img_cmn/schedule_03.gif" alt="灰色枠" width="59" height="21"></td>
<td width="76"><font color="#003366" class="text1">→共催公演</font></td>
</tr>
</tbody>
</table>



<table width="10" height="20" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td></td>
</tr>
</tbody></table>
</td>
<td width="142" valign="top"><img src="../img_cmn/head_03.jpg" width="142" height="71"></td>
</tr>
</tbody></table>
<center>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="687">
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="163" align="center" valign="middle" background="../img_cmn/schedule_04.gif"><font color="#003366" class="text3">2018月4月</font></td>
<td width="418"></td>
<td width="106" height="33"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="38" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201803.php"><img src="../img_cmn/schedule_06.gif" width="38" height="21" border="0"></a><?php endif; ?></td>
<td width="302" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($prevflag != 1):?><a href="./201803.php"><font color="#FFFFFF" class="text1">3月へ</font></a><?php endif; ?></td>
<td width="305" align="right" valign="middle" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201805.php"><font color="#FFFFFF" class="text1">5月へ</font></a><?php endif; ?></td>
<td width="42" align="right" background="../img_cmn/schedule_06_2.gif"><?php if($nextflag != 1):?><a href="./201805.php"><img src="../img_cmn/schedule_06_3.gif" width="42" height="21" border="0"></a><?php endif; ?></td>
</tr>
</tbody>
</table>



<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180401110722">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月 1日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>PROJECT B ２０１８</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>
<div><span>◆畑農敏哉（指揮）</span><br />◆田中良茂（ピアノ）</div>
</div>
<div>◆<a href="http://projectb.blog.so-net.ne.jp/" target="_blank">PROJECT B オーケストラ</a></div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ベートーヴェン：バレエ音楽「プロメテウスの創造物」序曲／交響曲第2番ニ長調op.36<br />
◆ブラームス：ピアノ協奏曲第1番ニ短調op.15</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1"><p>◆チケット発売日：２０１８年２月１日（木）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p>MAT音楽事務所 ０３-６６５７-５１５１</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180407151541">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月 7日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>山下洋輔 ソロピアノ・コンサート</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1">◆山下洋輔（ピアノ）</span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆曲目未定</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥５，５００（全指定）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年１月２７日（土）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1"><a href="http://t-onkyo.co.jp/" target="_blank">東京音協</a> ０３-５７７４-３０３０（平日11:00～17:00）<br /></span></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180408110601">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月 8日(日)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>オルケストラ シンフォニカ 東京　第５９回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div>◆嶋 直樹／石井啓之／山本雅三（指揮）</div>
<div>◆オルケストラ シンフォニカ 東京（マンドリンオーケストラ）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆チャイコフスキー：弦楽セレナーデより「ワルツ」<br />
◆武井守成：豊年<br />
◆ラッタ：英雄葬送曲<br />
◆ベルッティ：ハンガリアの黄昏<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆入場無料（全自由）<br />
</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://ostokyo.info/index.html" target="_blank">オルケストラ シンフォニカ 東京</a>（石井） ０９０-８９４４-０９６９</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180414151541">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月14日(土)</a><br></b></font>
<font class="text1"><br>13：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>天満敦子 ヴァイオリン・コンサート「春」</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><p><span class="text1">◆天満敦子（ヴァイオリン）<br /></span>◆酒井愛可（ピアノ）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆望郷のバラード<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥４，８００（全指定）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年２月２４日（土）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><span class="text1"><a href="http://t-onkyo.co.jp/" target="_blank">東京音協</a> ０３-５７７４-３０３０（平日11:00～17:00）<br /></span></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180415181059">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月15日(日)</a><br></b></font>
<font class="text1"><br>13：15 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>C＆V Orchesta Mandolino　第３５回定期演奏会</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><p><span style="color: #000000; line-height: 16px; font-family: 'ＭＳ ゴシック';">◆長谷川武宏（指揮）</span><br style="color: #000000; line-height: 16px; font-family: 'ＭＳ ゴシック';" /><span style="color: #000000; line-height: 16px; font-family: 'ＭＳ ゴシック';">◆C&amp;V Orchesta Mandolino（マンドリンオーケストラ）</span></p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆長谷川武宏：新曲（委嘱初演）／天空の奇蹟～マンドリンオーケストラの為の組曲～<br />
◆椎根真太郎：新曲（委嘱初演）<br />
◆二橋潤一：マンドリン協奏曲</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（当日自由）<br />
◆￥１，５００（前売自由）<br />
◆￥１，０００（学生自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1"><p>◆チケット発売日：２０１８年１月下旬予定</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><a href="http://orchesta.org/" target="_blank">C&amp;V Orchesta Mandolino</a>（髙橋）０９０-３２０４-６５２８</p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180421174609">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月21日(土)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>東京ヴィヴァルディ合奏団　第１１９回春の定期演奏会</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><p>◆東京ヴィヴァルディ合奏団（弦楽合奏）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ヴィヴァルディ：弦楽のための協奏曲RV157<br />
◆ボッシ：ゴルドーニ間奏曲<br />
◆ヴィヴァルディ：弦楽ための協奏曲RV152<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥６，０００（バルコニー指定）<br />
◆￥５，０００（Ｓ指定）<br />
◆￥４，０００（一般自由）<br />
◆￥２，０００（高校生以下指定／自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年１月２９日（月）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://vivaldi.jp" target="_blank">東京ヴィヴァルディ合奏団</a> ０３-６２７７-８４５０</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180422110902">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#E9E9E9">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月22日(日)</a><br></b></font>
<font class="text1"><br>15：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#E9E9E9">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>第６９回住友商事ヤング・シンフォニー　CLASSIC for KIDS ２０１８</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><div><span style="color: #000000;" color="#0066cc">◆山本祐ノ介（指揮とお話）</span></div>
<div>◆<a href="http://juniorphil.com/" target="_blank">ジュニア・フィルハーモニック・オーケストラ</a>（管弦楽）</div></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆エルガー：「威風堂々」第1番<br />
◆ロッシーニ：歌劇「ウィリアム・テル」序曲<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥１，０００（一般指定）<br />
◆￥５００（高校生以下指定）<br />
＊４歳以上入場可（チケットは１名につき１枚必要）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年３月１４日（水）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p>住友商事サステナビリティ推進部 ０３-５１６６-３４０１<a href="http://juniorphil.com/" target="_blank"><br /></a></p></font>
</td>
</tr>


<tr valign="top">
<td width="80" align="center"><font class="text1">【プレイガイド】</font></td>
<td width="410"><font class="text1"><a href="http://www.triton-arts.net/ja/" target="_blank">トリトンアーツ・チケットデスク</a> ０３-３５３２-５７０２</font></td>
</tr>


</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180428152506">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月28日(土)</a><br></b></font>
<font class="text1"><br>13：30 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b><p>女声合唱団「雪月花」&amp;「フィオレンティーナ」<br />第２回合同演奏会</p></b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><span class="text1">◆平林 龍（指揮・バリトン）<br />◆藤野沙優（ソプラノ）<br />◆高橋ちはる（メゾソプラノ）<br />◆竹内俊介（テノール）<br />◆雪月花／フィオレンティーナ（合唱）<br /></span></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆大中 恩：組曲「いぬのおまわりさん」<br />
◆平林 龍／下司愉宇起作詞：さがそうよ、幸せを<br />
◆モーツァルト：レクイエム ニ短調K626（女声合唱版）<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥２，０００（全自由）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【備　考】</font></td>
<td width="490"><font class="text1">◆チケット発売日：２０１８年１月５日（金）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><p><span class="text1"><a href="ttps://ryuhirabayashi.com/chorus/" target="_blank">平林 龍事務局</a> ０１２０-６５０-８７８</span></p></font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180429160401">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月29日(日)</a><br></b></font>
<font class="text1"><br>10：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>第６回 若い指揮者のための合唱指揮コンクール</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1">◆シグヴァルス・クラーヴァ／エルヴィン・オルトナー／グレーテ・ペダーシェン（審査員）<br />◆Ｅｎｓｅｍｂｌｅ ＰＶＤ（合唱）</font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆パレストリーナ：Ego sum panis vivus<br />
◆ブルックナー：Ave Maria <br />
◆メンデルスゾーン：Denn er hat seinen Engeln<br />
他</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆￥３，０００（一般自由）<br />
◆￥２，０００（学生自由）<br />
◆￥５００（高校生以下自由）</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://www.ongakuju.com/t-cantat/" target="_blank">Ｔｏｋｙｏ Ｃａｎｔａｔ</a>事務局 ０３-３９８５-０４０５</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0" id="20180430170526">
<tbody><tr>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="120" valign="top" bgcolor="#FFFFFF">
<table width="120" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td valign="top"><font class="text3"><b><a name="1765">4月30日(月)</a><br></b></font>
<font class="text1"><br>14：00 開演</font></td>
</tr>
</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
<td width="561" valign="top" bgcolor="#FFFFFF">
<table width="561" border="0" cellspacing="0" cellpadding="5">
<tbody><tr valign="top">
<td colspan="2"><font color="#000000" class="text1"><b>METT管弦楽団 第２２回スプリングコンサート</b></font></td>
</tr>

<tr valign="top">
<td width="71" align="center"><font class="text1">【出　演】</font></td>
<td width="490"><font class="text1"><p>◆寺田 研（ヴァイオリン）<br />◆久世武志（指揮）<br />◆METT管弦楽団（管弦楽）</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【曲　目】</font></td>
<td width="490"><font class="text1"><p>◆ベートーヴェン：レオノーレ序曲第3番op.72b<br />
◆ブルックナー：交響曲第4番変ホ長調WAB104「ロマンティック」</p></font></td>
</tr>


<tr valign="top">
<td width="71" align="center"><font class="text1">【料　金】</font></td>
<td width="490"><font class="text1"><p>◆入場無料（全自由）</p></font></td>
</tr>



<tr valign="top">
<td width="71" align="center"><font class="text1">【問合せ】</font></td>
<td width="490">
<font class="text1"><a href="http://mettorchestra.web.fc2.com/index.html" target="_blank">METT管弦楽団</a>（金澤） ０９０-４１６８-３９０９</font>
</td>
</tr>



</tbody></table></td>
<td width="2" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="10"></td>
</tr>
</tbody></table>
<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
<td width="2" rowspan="3" bgcolor="#0E7FAD"><img src="../img_cmn/spacer.gif" width="2" height="3"></td>
</tr>
<tr>
<td width="683" height="1"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td width="683" height="1" bgcolor="#006088"><img src="../img_cmn/spacer.gif" width="10" height="1"></td>
</tr>
</tbody></table>


<table width="687" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>

<td width="31" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201803.php"><img src="../img_cmn/schedule_07_1.gif" width="31" height="20" border="0"></a><?php endif;?></td>
<td width="309" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($prevflag != 1):?><a href="./201803.php"><font color="#FFFFFF" class="text2">3月へ</font></a><?php endif;?></td>

<td width="315" align="right" valign="middle" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201805.php"><font color="#FFFFFF" class="text2">5月へ</font></a><?php endif;?></td>
<td width="32" align="right" background="../img_cmn/schedule_07_2.gif"><?php if($nextflag != 1):?><a href="./201805.php"><img src="../img_cmn/schedule_07_3.gif" width="32" height="20" border="0"></a><?php endif;?></td>

</tr>
</tbody></table>



<table width="687" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td height="30" align="center" valign="middle"></td>
</tr><tr>
<td height="55" align="center" valign="middle"><font color="#003366" class="text2">|　<a href="../history/index.html">ホールの歴史</a>　|　<a href="../about-hall/index.html">施設概要</a>　|　<a href="../annai/index.html">ご利用案内</a>　|　<a href="../seatplan/index.html">座席表</a>　|　コンサートスケジュール　|　<a href="../access/index.html">アクセスガイド</a>　|</font></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
<table width="779" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td align="center" valign="middle" bgcolor="#01307E"><font color="#FFFFFF" class="text1">copylight(c)2003 The DAI-ICHI SEIMEI HALL all rights reserved.</font></td>
</tr>
<tr>
<td bgcolor="#000000"><img src="../img_cmn/spacer.gif" width="10" height="2"></td>
</tr>
</tbody></table>
</td>
<td width="38" background="../img_cmn/bg02.gif"><img src="../img_cmn/spacer.gif" width="38" height="1"></td>
</tr>
</tbody></table>
<table width="855" bgcolor="#E1E8F5" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td><img src="../img_cmn/spacer.gif" width="1" height="20"></td>
</tr>
</tbody></table>
</center>
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>