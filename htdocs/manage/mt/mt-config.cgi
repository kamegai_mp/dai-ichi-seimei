## Movable Type Configuration File
##
## This file defines system-wide
## settings for Movable Type. In 
## total, there are over a hundred 
## options, but only those 
## critical for everyone are listed 
## below.
##
## Information on all others can be 
## found at:
##  http://www.movabletype.jp/documentation/config

#======== REQUIRED SETTINGS ==========

CGIPath        /manage/mt/
StaticWebPath  /manage/mt/mt-static/
StaticFilePath /home/vuser12/9/4/0208749/www.dai-ichi-seimei-hall.jp/manage/mt/mt-static

#======== DATABASE SETTINGS ==========

ObjectDriver DBI::mysql
Database JxmuVXbFDv752
DBUser JxmuVXbFDv752
DBPassword Iqum8u5
DBHost mysql507.in.shared-server.net
DBPort 13212

#======== MAIL =======================
EmailAddressMain kawada@metaphase.co.jp
MailTransfer sendmail
SendMailPath /usr/sbin/sendmail
    
DefaultLanguage ja

ImageDriver ImageMagick

AllowFileInclude 1

HTMLUmask 0022
DirUmask 0022
HTMLPerms 0755
