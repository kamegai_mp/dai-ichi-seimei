package ListingFieldDateFormatChanger::CMS;

sub html {
    my ( $prop, $obj ) = @_;
    return MT::Util::format_ts( "%Y / %m / %d",
        $obj->authored_on, MT->app->blog,
        MT->app->user ? MT->app->user->preferred_language : undef );
}

sub html_un {
    my ( $prop, $obj ) = @_;
    return MT::Util::format_ts( "%Y / %m / %d",
        $obj->unpublished_on, MT->app->blog,
        MT->app->user ? MT->app->user->preferred_language : undef );
}

sub add_css {
    my ($cb, $app, $template) = @_;

    my $old;
    my $new;

    $old = <<HTML;
<mt:var name="html_head">
HTML
    $old = quotemeta($old);
    $new = <<HTML;
<mt:var name="html_head">
<style type="text/css">
.col.head.authored_on,
.col.head.unpublished_on {
    width: 150px;
}
</style>
HTML

    $$template =~ s/$old/$new/;

}

1;
