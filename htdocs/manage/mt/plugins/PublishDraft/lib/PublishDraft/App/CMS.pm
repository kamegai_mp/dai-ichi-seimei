package PublishDraft::App::CMS;

use strict;
use warnings;

use Encode;
use CustomFields::Field;

use MT::Util qw( is_valid_email is_url dirify offset_time_list format_ts );
use MT::I18N qw( wrap_text );
use CustomFields::Util qw( get_meta save_meta );
use PublishDraft::Util qw( get_lastest_revision get_lastest_revision_object get_lastest_revision_for_future_post get_lastest_revision_object_for_future_post );

use Data::Dumper;

sub pre_save {
    my ($eh, $app, $obj, $orig_obj) = @_;

    1;
}

sub save_snapshot_wf {
    my $app = shift;

    my $q     = $app->param;
    my $type  = $q->param('_type');
    my $id    = $q->param('id');
    my $blog_id    = $q->param('blog_id');
    my $param = {};

    return $app->errtrans("Invalid request.")
        unless $type;

    $app->validate_magic() or return;

    my $class = $app->model($type)
        or return $app->errtrans( "Invalid type [_1]", $type );

    return $app->error( $app->translate( "No ID" ) ) if !$id;

    my $revision = get_lastest_revision($app);
    return $app->error( "No Revision." ) if !$revision;

    my $obj = get_lastest_revision_object($app, $revision);
    return $app->error( $app->translate( "Invalid type [_1]", $type ) ) if $obj->class ne $type;

    my $original = $obj->clone();
#    $original->{__tags} = $obj->{__tags};

    my @category_list;
    my $categories = $obj->cache_property( 'categories' );
    foreach my $c (@{$categories}) {
        push @category_list, $c->id;
    }
#    $original->{__cats} = \@category_list;

    my $names    = $obj->column_names;
    my %values   = map { $_ => ( scalar $q->param($_) ) } @$names;

    if ( ( 'entry' eq $type ) || ( 'page' eq $type ) ) {
        $app->_translate_naughty_words($obj);
    }
    delete $values{'id'} if exists( $values{'id'} ) && !$values{'id'};
    $obj->set_values( \%values );

    my $ao_d = $q->param('authored_on_date');
    my $ao_t = $q->param('authored_on_time');
##################### MT6
    my $uo_d = $app->param('unpublished_on_date');
    my $uo_t = $app->param('unpublished_on_time');
##################### MT6
    my $ts;
    if ( $ao_d ) {
        my %param = ();
        my $ao    = $ao_d . ' ' . $ao_t;
        unless ( $ao
            =~ m!^(\d{4})-(\d{1,2})-(\d{1,2})\s+(\d{1,2}):(\d{1,2})(?::(\d{1,2}))?$!
            )
        {
            $param{error} = $app->translate(
                "Invalid date '[_1]'; 'Published on' dates must be in the format YYYY-MM-DD HH:MM:SS.",
                $ao
            );
        }
        unless ( $param{error} ) {
            my $s = $6 || 0;
            $param{error} = $app->translate(
                "Invalid date '[_1]'; 'Published on' dates should be real dates.",
                $ao
                )
                if (
                   $s > 59
                || $s < 0
                || $5 > 59
                || $5 < 0
                || $4 > 23
                || $4 < 0
                || $2 > 12
                || $2 < 1
                || $3 < 1
                || ( MT::Util::days_in( $2, $1 ) < $3
                    && !MT::Util::leap_day( $0, $1, $2 ) )
                );
        }
        $param{return_args} = $app->param('return_args');
        return $app->forward( "view", \%param ) if $param{error};
        $ts = sprintf "%04d%02d%02d%02d%02d%02d", $1, $2, $3, $4, $5, ( $6 || 0 );
        $obj->authored_on($ts);
    }
##################### MT6
    if ( $uo_d || $uo_t ) {
        my %param = ();
        my $uo    = $uo_d . ' ' . $uo_t;
        $param{error} = $app->translate(
            "Invalid date '[_1]'; 'Unpublished on' dates must be in the format YYYY-MM-DD HH:MM:SS.",
            $uo
            )
            unless ( $uo
            =~ m!^(\d{4})-(\d{1,2})-(\d{1,2})\s+(\d{1,2}):(\d{1,2})(?::(\d{1,2}))?$!
            );
        unless ( $param{error} ) {
            my $s = $6 || 0;
            $param{error} = $app->translate(
                "Invalid date '[_1]'; 'Unpublished on' dates should be real dates.",
                $uo
                )
                if (
                   $s > 59
                || $s < 0
                || $5 > 59
                || $5 < 0
                || $4 > 23
                || $4 < 0
                || $2 > 12
                || $2 < 1
                || $3 < 1
                || ( MT::Util::days_in( $2, $1 ) < $3
                    && !MT::Util::leap_day( $0, $1, $2 ) )
                );
        }
        my $ts = sprintf "%04d%02d%02d%02d%02d%02d", $1, $2, $3, $4, $5,
            ( $6 || 0 );
        require MT::DateTime;
        unless ( $param{error} ) {
            $param{error} = $app->translate(
                "Invalid date '[_1]'; 'Unpublished on' dates should be dates in the future.",
                $uo
                )
                if (
                MT::DateTime->compare(
                    blog => $app->blog,
                    a    => { value => time(), type => 'epoch' },
                    b    => $ts
                ) > 0
                );
        }
        if ( !$param{error} && $obj->authored_on ) {
            $param{error} = $app->translate(
                "Invalid date '[_1]'; 'Unpublished on' dates should be later than the corresponding 'Published on' date.",
                $uo
                )
                if (
                MT::DateTime->compare(
                    blog => $app->blog,
                    a    => $obj->authored_on,
                    b    => $ts
                ) > 0
                );
        }
        $param{show_input_unpublished_on} = 1 if $param{error};
        $param{return_args} = $app->param('return_args');
        return $app->forward( "view", \%param ) if $param{error};
        $obj->unpublished_on($ts);
    }
    else {
        $obj->unpublished_on(undef);
    }
##################### MT6

    $obj->current_revision($revision->rev_number);

#    $obj->status(10) if $obj->status == 40;

    require MT::Asset;
    require MT::ObjectAsset;
    my $include_asset_ids = $app->param('include_asset_ids');
    my @asset_ids         = split( ',', $include_asset_ids );
    my $obj_assets        = ();
    my @obj_assets        = MT::ObjectAsset->load(
        { object_ds => 'entry', object_id => $obj->id } );
    foreach my $obj_asset (@obj_assets) {
        my $asset_id = $obj_asset->asset_id;
        $obj_assets->{$asset_id} = 1;
    }
    my $seen = ();
    foreach my $asset_id (@asset_ids) {
        my $obj_asset = MT::ObjectAsset->load(
            {   asset_id  => $asset_id,
                object_ds => 'entry',
                object_id => $obj->id
            }
        );
        unless ($obj_asset) {
            my $obj_asset = new MT::ObjectAsset;
            $obj_asset->blog_id($blog_id);
            $obj_asset->asset_id($asset_id);
            $obj_asset->object_ds('entry');
            $obj_asset->object_id( $obj->id );
            $obj_asset->save;
        }
        $seen->{$asset_id} = 1;
    }
    foreach my $asset_id ( keys %{$obj_assets} ) {
        unless ( $seen->{$asset_id} ) {
            my $obj_asset = MT::ObjectAsset->load(
                {   asset_id  => $asset_id,
                    object_ds => 'entry',
                    object_id => $obj->id
                }
            );
            $obj_asset->remove;
        }
    }

    my $cat;
    my $cat_ids = $q->param('category_ids');
    if ($cat_ids) {
        my @cats = split /,/, $cat_ids;
        if (@cats) {
            my $primary_cat = $cats[0];
            $cat = MT::Category->load(
                { id => $primary_cat, blog_id => $blog_id } );
            my @categories
                = MT::Category->load( { id => \@cats, blog_id => $blog_id } );
            $obj->cache_property( 'category',   undef, $cat );
            $obj->cache_property( 'categories', undef, \@categories );
        }
    }
    else {
        $obj->cache_property( 'category', undef, undef );
        $obj->cache_property( 'categories', undef, [] );
    }

    if ($q->param('tags')) {
        my @tag_names = split(/\s*,\s*/,$q->param('tags'));
        my @save_tags;
        foreach my $tag_name (@tag_names) {
            my $tag;
            my @tags = MT::Tag->load({name => $tag_name});
            if (!@tags) {
                $tag = MT::Tag->new;
                $tag->name($tag_name);
                $tag->save;
            } else {
               $tag = shift @tags;
            }
            push(@save_tags, $tag);
        }
        $obj->{__tags} = [ map { $_->name } @save_tags ];
        $obj->{__tag_objects} = \@save_tags;
    } else {
        $obj->{__tags} = [];
        $obj->{__tag_objects} = [];
    }

    _set_customfield_objs($app, $obj);

    $obj->gather_changed_cols( $original, $app );
# don't use
#    $app->run_callbacks( 'cms_post_save.' . $type, $app, $obj, $original );

    if ($obj->{changed_revisioned_cols}) {
        my $col = 'max_revisions_' . $obj->datasource;
        if ( my $blog = $obj->blog ) {
            my $max = $blog->$col;
            $obj->handle_max_revisions($max);
        }
        my $revision = $obj->save_revision( $q->param('revision-note') );
    }

    if ($obj->{changed_revisioned_cols}) {
        $app->add_return_arg( 'saved_snapshot' => 1 );
    } else {
        $app->add_return_arg( 'no_snapshot' => 1 );
    }

    my $entry = MT::Entry->load( $id );
    my $status = $q->param('status');

    if ( $status == 100 ) {
        $entry->status(2);
        $entry->future_post(1);
        $entry->futured_on($ts);
        $entry->save;
    } else {
        $entry->future_post(0);
        $entry->futured_on('');
        $entry->save;
    }

    $app->add_return_arg( 'id' => $obj->id ) if !$original->id;
    $app->call_return;
}

sub gather_changed_cols {
    my ( $cb, $obj, $orig_obj, $app ) = @_;

    my @changed_cols;
    push @changed_cols, 'publish_draft';

    $obj->{changed_revisioned_cols} = \@changed_cols;

    1;
}

sub _gather_changed_cols {
    my ( $obj, $orig_obj, $app ) = @_;

    my @changed_cols;
    my $revisioned_cols = $obj->revisioned_columns;

    my %date_cols = map { $_ => 1 }
        @{ $obj->columns_of_type( 'datetime', 'timestamp' ) };

    foreach my $col (@$revisioned_cols) {
        next if $orig_obj && $obj->$col eq $orig_obj->$col;
        next
            if $orig_obj
                && exists $date_cols{$col}
                && $orig_obj->$col eq MT::Object::_db2ts( $obj->$col );

        push @changed_cols, $col;
    }

#    $obj->{changed_revisioned_cols} = \@changed_cols
#        if @changed_cols;
#    return 1 if @changed_cols;

    my $tag_changed = 0;

    my @objecttags = @{ $orig_obj->{__tags} };

    my @tag_names = split /\s*,\s*/,
            ( $app->param('tags') || '' );

    $tag_changed = 1 if scalar(@tag_names) != scalar(@objecttags);

    unless ($tag_changed) {
        if (@tag_names && @objecttags) {
            my @sort_tag_names = sort { $a <=> $b } @tag_names;
            my @sort_objecttags = sort { $a <=> $b } @objecttags;
            $tag_changed = 1 if join('', @sort_tag_names) ne join('', @sort_objecttags);
        }
    }
    push @changed_cols, 'tags' if $tag_changed;

    my $cat_changed = 0;

    my @category_list = @{ $orig_obj->{__cats} };

    my @category_ids;
    eval {
        @category_ids = split /\s*,\s*/,
            ( $app->param('category_ids') || '' );
        @category_ids = grep { $_ != -1 } @category_ids;
    };

    $cat_changed = 1 if scalar(@category_ids) != scalar(@category_list);

    unless ($cat_changed) {
        if (@category_ids && @category_list) {
            my @sort_category_ids = sort { $a <=> $b } @category_ids;
            my @sort_category_list = sort { $a <=> $b } @category_list;
            $cat_changed = 1 if join('', @sort_category_ids) ne join('', @sort_category_list);
        }
    }

    push @changed_cols, 'categories' if $cat_changed;

#    object asset
#    my $orig_object_assets = $obj->cache_property( 'orig_object_assets' );
#    my $object_assets = $obj->cache_property( 'object_assets' );
#    return 1 if !$orig_object_assets && !$object_assets;

#    my @orig_assets = sort { $a <=> $b } @$orig_object_assets;
#    my @assets = sort { $a <=> $b } @$object_assets;

#    $obj->cache_property( 'orig_object_assets', undef, undef );
#    return 1 if join('', @orig_assets) == join('', @assets);

#    my $changed_cols = $obj->{changed_revisioned_cols};
#    push @$changed_cols, 'assets';

#    return 1 if @changed_cols;

    my $meta = get_meta($orig_obj);
    while ( my ( $key, $val ) = each %$meta ) {
        if ( my $newval = $app->param( 'customfield_' . $key ) ) {
            if ( $val ne $newval ) {
                push @changed_cols, "field.$key";
            }
        } else {
            my $field = CustomFields::Field->load(
                {   $app->blog->id
                    ? ( blog_id => [ $app->blog->id, 0 ] )
                    : ( blog_id => $app->blog->id ),
                    basename => $key
                });
            if( $field ) {
                if ( $field->type eq 'datetime' ) {
                    if ( $field->options eq 'datetime' ) {
                        $newval = $app->param( 'd_customfield_' . $key ) . " " .
                                  $app->param( 't_customfield_' . $key );
                    } elsif ( $field->options eq 'date' ) {
                        $newval = $app->param( 'd_customfield_' . $key );
                        $val =~ s/(\d{4}-\d{2}-\d{2}) \d{2}:\d{2}:\d{2}/$1/;
                    } elsif ( $field->options eq 'time' ) {
                        $newval = $app->param( 't_customfield_' . $key );
                        $val =~ s/\d{4}-\d{2}-\d{2} (\d{2}:\d{2}:\d{2})/$1/;
                    }
                    if ( $val ne $newval ) {
                        push @changed_cols, "field.$key";
                    }
                } else {
                    push @changed_cols, "field.$key" if $val;
                }
            }
        }
    }

    $obj->{changed_revisioned_cols} = \@changed_cols
        if @changed_cols;

    1;
}

sub _set_customfield_objs {
    my ( $app, $obj ) = @_;
    return 1 unless $app->isa('MT::App');

    my $q = $app->param;
    my $blog_id = $app->param('blog_id') || 0;

    return 1 if !$q->param('customfield_beacon');
    require CustomFields::Field;

    my $meta = {};
    my %date_fields;
    foreach ( $q->param() ) {
        next if $_ eq 'customfield_beacon';
        if (m/^customfield_(.*?)$/) {
            my $field_name = $1;
            if (m/^customfield_(.+?)_cb_beacon$/) {
                $field_name = $1;
                $meta->{$field_name}
                    = defined( $q->param("customfield_$field_name") )
                    ? $q->param("customfield_$field_name")
                    : '0';
            }
            else {
                my $field = CustomFields::Field->load(
                    {   $blog_id
                        ? ( blog_id => [ $blog_id, 0 ] )
                        : ( blog_id => $blog_id ),
                        basename => $field_name
                    }
                ) or next;
                $meta->{$field_name} = $q->param("customfield_$field_name");
            }
        } elsif (m/^([td]_)customfield_(.*?)$/) {
            my ( $td, $field_name ) = ( $1, $2 );
            if ( $td ) {
                $date_fields{$field_name} = 1;
            }
        }

    }
    for my $field_name ( keys %date_fields ) {
        my $field = CustomFields::Field->load(
            {   $blog_id
                ? ( blog_id => [ $blog_id, 0 ] )
                : ( blog_id => $blog_id ),
                basename => $field_name
            }
        ) or next;
        if ( $field->options eq 'datetime' ) {
            $meta->{$field_name} = $q->param("d_customfield_$field_name") . " " .
                                   $q->param("t_customfield_$field_name");
        }
        elsif ( $field->options eq 'date' ) {
            if ( $q->param("d_customfield_$field_name")) {
                $meta->{$field_name} = $q->param("d_customfield_$field_name") . ' 00:00:00';
            } else {
                $meta->{$field_name} = '';
            }
        }
        elsif ( $field->options eq 'time' ) {
            if ( $q->param("t_customfield_$field_name")) {
                $meta->{$field_name} = '1970-01-01 ' . $q->param("t_customfield_$field_name");
            } else {
                $meta->{$field_name} = '';
            }
        }
    }
    _save_meta_wf( $obj, $meta ) if %$meta;

    1;
}

sub _save_meta_wf {
    my $plugin = MT->component("Commercial");
    my ( $obj, $meta ) = @_;
    return 0 unless $obj;
    my $obj_type
        = $obj->can('class_type') ? $obj->class_type : $obj->datasource;

    if ( $obj->has_meta ) {
        foreach my $mf ( keys %$meta ) {
            if ( $obj->has_meta( 'field.' . $mf ) ) {
                $obj->meta( 'field.' . $mf, $meta->{$mf} );
            }
        }
    }
    else {
        my $id = $obj->id;

        require MT::PluginData;
        my $meta_data = MT::PluginData->get_by_key(
            { plugin => 'CustomFields', key => "${obj_type}_${id}" } );
        $meta_data->data( { customfields => $meta } );
    }
    sync_assets($obj) unless $obj->isa('MT::Entry');

    return 1;
}

sub pack_revision_asset {
    my $obj = shift;
    my ($values) = @_;

    $values->{__rev_assets} = $obj->cache_property( 'object_assets' );
    $values;
}

sub unpack_revision_asset {
    my $obj = shift;
    my ($packed_obj) = @_;

    if ( my $rev_assets = delete $packed_obj->{__rev_assets} ) {
        $obj->clear_cache('assets');
        $obj->cache_property( 'object_assets', undef, \@$rev_assets );
    }
}

sub plugin_publish_future_posts {
    my $this = shift;

    require MT::Blog;
    require MT::Entry;
    require MT::Util;
    my $mt            = MT->instance;
    my $total_changed = 0;
    my @blogs         = MT::Blog->load(
        { class => '*' },
        {   join => MT::Entry->join_on(
                'blog_id',
                { status => 2, },
                { future_post => 1, },
                { unique => 1 }
            )
        }
    );

    foreach my $blog (@blogs) {
        my @ts = MT::Util::offset_time_list( time, $blog );
        my $now = sprintf "%04d%02d%02d%02d%02d%02d", $ts[5] + 1900,
            $ts[4] + 1,
            @ts[ 3, 2, 1, 0 ];
        my $iter = MT::Entry->load_iter(
            {   blog_id => $blog->id,
                status  => 2,
                future_post => 1,
                class   => '*'
            },
            {   'sort'    => 'futured_on',
                direction => 'descend'
            }
        );
        my @queue;
        while ( my $entry = $iter->() ) {
            push @queue, $entry->id if $entry->futured_on le $now;
        }

        my $changed = 0;
        my @results;
        my %rebuild_queue;
        my %ping_queue;
        foreach my $entry_id (@queue) {

            my $entry = MT::Entry->load($entry_id)
                or next;

            my $revision = get_lastest_revision_for_future_post($mt, $entry->class, $entry_id);
                next if !$revision;

            my $obj = get_lastest_revision_object_for_future_post($mt, $entry->class, $entry_id, $revision);
                next if !$obj;

            # set $obj -> $entry
            my $values = $obj->get_values;
            $entry->set_values( $values );

            # set $obj -> $entry tag
            my @tags = @{ $obj->{__tags} };
            if (@tags) {
                $obj->set_tags(@tags);
            } else {
                $obj->remove_tags();
            }

            # set $obj -> $entry custom field
            if ( $obj->has_meta ) {
                my $meta_data = get_meta($obj);
                save_meta( $entry, $meta_data ) if %$meta_data;
            }

            # set $obj -> $entry category
            #カテゴリの設定未
            $obj->cache_property( 'category' );
            $obj->cache_property( 'categories' );

            # set $obj -> $entry asset
            my $assets;
            my $asset_ids = $obj->cache_property( 'object_assets' );
            foreach my $asset_id (@{$asset_ids}) {
                my $asset = MT::Asset->load( { object_id => $obj->id, asset_id => $asset_id } );
                next if $asset;
                $asset = MT::Asset->new;

            # assetがない場合の設定が未
                my $asset_1;
                if ( $asset->class eq 'image' ) {
                    $asset_1 = {
                        asset_id    => $asset->id,
                        asset_name  => $asset->file_name,
                        asset_thumb => $asset->thumbnail_url( Width => 100 ),
                        asset_type  => $asset->class
                    };
                } else {
                    $asset_1 = {
                        asset_id   => $asset->id,
                        asset_name => $asset->file_name,
                        asset_type => $asset->class
                    };
                }
                push @{$assets}, $asset_1;
            }

            $entry->status( MT::Entry::RELEASE() );
            $entry->future_post( 0 );
            $entry->save
                or die $entry->errstr;

            my $col = 'max_revisions_' . $entry->datasource;
            if ( my $blog = $entry->blog ) {
                my $max = $blog->$col;
                $entry->handle_max_revisions($max);
            }
            my @changed_cols;
            push @changed_cols, 'status';
            $entry->{changed_revisioned_cols} = \@changed_cols;

            # for revision number hack
            my $current_revision = $entry->current_revision;
            $entry->current_revision( $current_revision + 1 );
            my $plugin = MT->component('PublishDraft');
            $revision = $entry->save_revision( $plugin->translate('by Schduled Task') );
            $entry->current_revision($revision);
            $entry->save
                or die $entry->errstr;

            MT->run_callbacks( 'scheduled_post_published', $mt, $entry );

            $rebuild_queue{ $entry->id } = $entry;
            $ping_queue{ $entry->id }    = 1;
            my $n = $entry->next(1);
            $rebuild_queue{ $n->id } = $n if $n;
            my $p = $entry->previous(1);
            $rebuild_queue{ $p->id } = $p if $p;
            $changed++;
            $total_changed++;
        }
        if ($changed) {
            my %rebuilt_okay;
            my $rebuilt;
            eval {
                foreach my $id ( keys %rebuild_queue )
                {
                    my $entry = $rebuild_queue{$id};
                    $mt->rebuild_entry( Entry => $entry, Blog => $blog )
                        or die $mt->errstr;
                    $rebuilt_okay{$id} = 1;
                    if ( $ping_queue{$id} ) {
                        $mt->ping_and_save( Entry => $entry, Blog => $blog );
                    }
                    $rebuilt++;
                }
                $mt->rebuild_indexes( Blog => $blog )
                    or die $mt->errstr;
            };
            if ( my $err = $@ ) {

                # a fatal error occured while processing the rebuild
                # step. LOG the error and revert the entry/entries:
                require MT::Log;
                $mt->log(
                    {   message => $mt->translate(
                            "An error occurred while publishing scheduled entries: [_1]",
                            $err
                        ),
                        class    => "publish",
                        category => 'rebuild',
                        blog_id  => $blog->id,
                        level    => MT::Log::ERROR()
                    }
                );
                foreach my $id (@queue) {
                    next if exists $rebuilt_okay{$id};
                    my $e = $rebuild_queue{$id};
                    next unless $e;
                    $e->status( 100 );
                    $e->future_post( 1 );
                    $e->save or die $e->errstr;
                }
            }
        }
    }
    $total_changed > 0 ? 1 : 0;
}

1;
