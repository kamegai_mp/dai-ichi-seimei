package PublishDraft::Template::Tags;

use MT::Entry;

sub hdlr_entry_status {
    my ($ctx) = @_;
    my $entry = $ctx->stash('entry')
        or return $ctx->_no_entry_error();
    my $blog_id = $ctx->stash('blog_id');

    my $plugin = MT->component('PublishDraft');
    my $show_status_for_admin = $plugin->get_config_value('workflow_show_status_for_admin', 'blog:'.$blog_id);
    my $show_status_for_users = $plugin->get_config_value('workflow_show_status_for_users', 'blog:'.$blog_id);

    my $app = MT->app;
    my $author = $app->user;

    my $reviewee = $app->can_do('request_entry_approval');
    my $reviewer = $app->can_do('approve_entry');

    if ($author->is_superuser()) {
        return MT::Entry::status_text($entry->status) if !$show_status_for_admin;
    } else {
        unless ($reviewer || $reviewee) {
            return MT::Entry::status_text($entry->status) if !$show_status_for_users;
        }
    }
    my $type = 'entry';
    my ($terms, $args);
    $terms->{entry_id} = $entry->id;
    $args->{sort} = 'rev_number';
    $args->{direction} = 'descend';
    $args->{limit} = 1;
    my $rev_model = $app->model($type.':revision');
    my @revs = $rev_model->load($terms, $args);
    my $revision = shift @revs || undef;
    return MT::Entry::status_text($entry->status) if !$revision;

    my @objs = $app->model($type)->load($entry->id);
    my $obj = shift @objs || undef;
    return MT::Entry::status_text($entry->status) if !$obj;

    my $rev = $obj->load_revision( { rev_number => $revision->rev_number } );
    if ( $rev && @$rev ) {
        $obj = $rev->[0];
    }
    return _status_text($obj->status);
}

sub _status_text {
    my ($s) = @_;

    return "Draft" if $s == 1;
    return "Publish" if $s == 2;
    return "Review" if $s == 3;
    return "Future" if $s == 4;
    return "Spam" if $s == 5;
    return "Publish-Draft" if $s == 10;
    return "Publish-Review" if $s == 20;
}

1;

