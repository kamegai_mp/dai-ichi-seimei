package PublishDraft::CMS::Entry;

use strict;
use warnings;

use Encode;
use CustomFields::App::CMS;

use MT::Util qw( format_ts relative_date );
use CustomFields::Util qw( get_meta _get_html );
use PublishDraft::Util qw( get_lastest_revision );

use Data::Dumper;

sub preview_entry {
    my ($cb, $app, $template) = @_;

    my $old;
    my $new;

    $old = <<HTML;
  <button
     mt:mode="save_entry"
     type="submit"
     name="save"
     value="save"
     accesskey="s"
     title="<\$mt:var name="save_button_title"\$>"
     class="save action primary button">
    <mt:var name="save_button_value">
  </button>
HTML
    $old = quotemeta($old);

    $new = <<HTML;
HTML

    $$template =~ s/$old//;

}

sub list_revision {
    my ($cb, $app, $terms, $args, $param, $hasher) = @_;

    my $q = $app->param;
    my $r = $q->param('r');

    return if $q->param('_type') eq 'template';
    my $id = $terms->{entry_id};
    my $obj = MT::Entry->load($id);
    my $type = 'entry';
    my $blog = $app->blog;
    my $lang  = $app->user ? $app->user->preferred_language : undef;
    my %users;
    my $js = $param->{rev_js};
    unless ($js) {
        $js
            = "location.href='"
            . $app->uri
            . '?__mode=view&amp;_type='
            . $app->param('_type');
        if ( my $id = $obj->id ) {
            $js .= '&amp;id=' . $id;
        }
        if ( defined $blog ) {
            $js .= '&amp;blog_id=' . $blog->id;
        }
    }
    $$hasher = sub {
        my ( $rev, $row ) = @_;

        if ( my $ts = $rev->created_on ) {
            $row->{created_on_formatted}
                = format_ts( MT::App::CMS::LISTING_DATE_FORMAT(),
                $ts, $blog, $lang );
            $row->{created_on_time_formatted}
                = format_ts( MT::App::CMS::LISTING_TIMESTAMP_FORMAT(),
                $ts, $blog, $lang );
            $row->{created_on_relative} = relative_date( $ts, time, $blog );
        }
        if ( $row->{created_by} ) {
            my $created_user = $users{ $row->{created_by} }
                ||= MT::Author->load( $row->{created_by} );
            if ($created_user) {
                $row->{created_by} = $created_user->nickname;
            }
            else {
                $row->{created_by} = $app->translate('(user deleted)');
            }
        }
        my $revision    = $obj->object_from_revision($rev);
        my $column_defs = $obj->column_defs;

        if ( ( 'entry' eq $type ) || ( 'page' eq $type ) ) {
            $row->{rev_status} = $revision->[0]->status;
        }
        $row->{rev_js}  = $js . '&amp;r=' . $row->{rev_number} . "'";

        my ($terms, $args);
        $terms->{'entry_id'} = $revision->[0]->id;
        $args->{sort} = 'rev_number';
        $args->{direction} = 'descend';
        $args->{limit} = 1;
        my $rev_model = MT->instance->model($type.':revision');
        my @revs = $rev_model->load($terms, $args);
        my $revision = shift @revs || undef;

        if ($r) {
            $row->{is_current} = $r == $row->{rev_number};
        } else {
            $row->{is_current} = $revision->rev_number == $row->{rev_number};
        }
    };

}

sub set_entry_param {
    my ($cb, $app, $param, $tmpl) = @_;

    my $author = $app->user;

    my $reviewee = $app->can_do('request_entry_approval');
    my $reviewer = $app->can_do('approve_entry');

    my $q = $app->param;
    my $type = $q->param('_type');

    my $id = $q->param('id');
    my $model = $app->model($type);
    my $rev_model = $app->model($type.':revision');
    return 1 unless $rev_model;

    my $obj;
    my ($terms, $args);
    $terms->{entry_id} = $id;
    $args->{sort} = 'rev_number';
    $args->{direction} = 'descend';

    ### preview bug fix
    if (!$q->param('reedit')) {
    ###

#    if (!$author->is_superuser() && ($reviewee || $reviewer)) {
        $args->{limit} = 1;
        my @revs = $rev_model->load($terms, $args);

        my $revision = shift @revs || undef;
        $revision = get_lastest_revision($app) if !$revision;

        if ($revision) {
            my $revision_number = $q->param('r') ? $q->param('r') : $revision->rev_number;

            my @objs = $model->load($id);
            $obj = shift @objs || undef;
            my $rev = $obj->load_revision( { rev_number => $revision_number } );
            if ( $rev && @$rev ) {
                $obj = $rev->[0];

                $param->{entry} = $obj;

                my $values = $obj->get_values;
                $param->{$_} = $values->{$_} foreach keys %$values;
                $param->{rev_date} = format_ts( "%Y-%m-%d %H:%M:%S",
                    $obj->modified_on, $app->blog,
                    $app->user ? $app->user->preferred_language : undef );

######################
        my $blog = MT::Blog->load( $q->param('blog_id') );
        $param->{'authored_on_date'} = $q->param('authored_on_date')
            || format_ts( "%Y-%m-%d", $param->{authored_on}, $blog,
            $app->user ? $app->user->preferred_language : undef );
        $param->{'authored_on_time'} = $q->param('authored_on_time')
            || format_ts( "%H:%M:%S", $param->{authored_on}, $blog,
            $app->user ? $app->user->preferred_language : undef );
######################
##################### MT6
        $param->{'unpublished_on_date'} = format_ts( "%Y-%m-%d", $obj->unpublished_on, $app->blog,
            $app->user ? $app->user->preferred_language : undef );
        $param->{'unpublished_on_time'} = format_ts( "%H:%M:%S", $obj->unpublished_on, $app->blog,
            $app->user ? $app->user->preferred_language : undef );
##################### MT6
            }

            $param->{tags} = join(',',@{$obj->{__tags}});

            my $cat_id;
            my @category_list;
            my %places;

            my $primary = $obj->cache_property( 'category' );
            push @category_list, $primary->id if $primary;

            my $secondary = $obj->cache_property( 'categories' );
            foreach my $c (@{$secondary}) {
                push @category_list, $c->id;
            }

            my $cats = join(',',@category_list);
            if ( defined $cats ) {
                if ( my @cats = grep { $_ =~ /^\d+/ } split /,/, $cats ) {
                    $cat_id = $cats[0];
                    %places = map { $_ => 1 } @cats;
                }
            }
            my $data = $app->_build_category_list(
                blog_id => $q->param('blog_id'),
                markers => 1,
                type    => $model->container_type,
            );
            my $top_cat = $cat_id;
            my @sel_cats;
            my $cat_tree = [];
            if ( $type eq 'page' ) {
                push @$cat_tree,
                    {
                    id       => -1,
                    label    => '/',
                    basename => '/',
                    path     => [],
                    };
                $top_cat ||= -1;
            }
            foreach (@$data) {
                next unless exists $_->{category_id};
                if ( $type eq 'page' ) {
                    $_->{category_path_ids} ||= [];
                    unshift @{ $_->{category_path_ids} }, -1;
                }
                push @$cat_tree,
                    {
                    id       => $_->{category_id},
                    label    => $_->{category_label} . ( $type eq 'page' ? '/' : '' ),
                    basename => $_->{category_basename}
                        . ( $type eq 'page' ? '/' : '' ),
                    path   => $_->{category_path_ids} || [],
                    fields => $_->{category_fields}   || [],
                    };
                push @sel_cats, $_->{category_id}
                    if $places{ $_->{category_id} }
                        && $_->{category_id} != $cat_id;
            }
            $param->{category_tree} = $cat_tree;
            unshift @sel_cats, $top_cat if defined $top_cat && $top_cat ne "";
            $param->{selected_category_loop}   = \@sel_cats;
            $param->{have_multiple_categories} = scalar @$data > 1;

            if ( $rev && @$rev ) {
                if ($q->param('r')) {
                    my @objs2 = $model->load($id);
                    my $obj2 = shift @objs2 || undef;
                    my $rev2 = $obj2->load_revision( { rev_number => $revision->rev_number } );
                    my $obj2 = $rev2->[0] if ( $rev2 && @$rev2 );
                    $param->{status} = $obj2->status;
                    $param->{rev_number} = $q->param('r');
                    if ($revision->rev_number == $q->param('r')) {
                        $param->{loaded_revision} = 0;
                    } else {
                        $param->{loaded_revision} = 1;
                    }
                }
            }

            my @obj_assets = MT::ObjectAsset->load(
                          {
                              object_id => $obj->id
                          } );
            my @asset_ids;
            if (@obj_assets) {
                @asset_ids = map { $_->asset_id } @obj_assets;
            }

            my $assets = [];
            foreach my $asset_id (@asset_ids) {
                my $asset = MT::Asset->load($asset_id);
                next if !$asset;

                my $asset_1;
                if ( $asset->class eq 'image' ) {
                    $asset_1 = {
                        asset_id    => $asset->id,
                        asset_name  => $asset->file_name,
                        asset_thumb => $asset->thumbnail_url( Width => 100 ),
                        asset_type  => $asset->class
                    };
                }
                else {
                    $asset_1 = {
                        asset_id   => $asset->id,
                        asset_name => $asset->file_name,
                        asset_type => $asset->class
                    };
                }
                push @{$assets}, $asset_1;
            }
            $param->{asset_loop} = $assets;

            $param->{'saved_snapshot'} = 1 if $q->param('saved_snapshot');
        }
#    } else {
#        my @objs = $model->load($id);
#        $obj = shift @objs || undef;
#
#        if ($q->param('r')) {
#            my $rev = $obj->load_revision( { rev_number => $q->param('r') } );
#            if ( $rev && @$rev ) {
#                $obj = $rev->[0];
#                $param->{rev_date} = format_ts( "%Y-%m-%d %H:%M:%S",
#                    $obj->modified_on, $app->blog,
#                    $app->user ? $app->user->preferred_language : undef ) if $obj;
#            }
#        }
#    }

    ### preview bug fix
    }
    ###
MT->instance->log("Status:".$param->{status});
#######################
    if ($param->{status} == 10) {
        $param->{status_publish_draft} = 1;
        $param->{status_draft} = 0;
    }
    if ($param->{status} == 20) {
        $param->{status_draft} = 0;
    }
    if ($param->{status} == 100) {
        $param->{status_publish_future} = 1;
    }
#######################

    CustomFields::App::CMS::param_edit_entry_post_types( @_ );

    my $commercial = MT->component('commercial');

    # YAY DOM
    # Add the custom fields to the customizable_fields and custom_fields javascript variables
    # for Display Options toggline
    my $header = $tmpl->getElementById('header_include');
    my $html_head = $tmpl->createElement('setvarblock', { name => 'html_head', append => 1 });
    my $innerHTML = q{
<script type="text/javascript">
/* <![CDATA[ */
    var customFieldByCategory = new Array();
    var customFields = new Object();
    <mt:loop name="field_loop"><mt:if name="field_id" like="^customfield_">customFields['<mt:var name="id">'] = '<mt:var name="field_id">';</mt:if><mt:if name="required">default_fields.push('<mt:var name="field_id">');</mt:if>
    </mt:loop>
/* ]]> */
</script>
};
    $html_head->innerHTML($innerHTML);
    $tmpl->insertBefore($html_head, $header);

    my $footer = $tmpl->getElementById('footer_include');
    my $html_foot = $tmpl->createElement('setvarblock', { name => 'html_body_footer', append => 1 });
    my $static_uri = $app->static_path;
    $innerHTML = qq(
<script type="text/javascript" src="${static_uri}addons/Commercial.pack/js/customfield_header.js"></script>
);
    $html_foot->innerHTML($innerHTML);
    $tmpl->insertBefore($html_foot, $footer);

    $param->{__fields_pre_loaded__} = 1
        if $param->{loaded_revision} && $param->{rev_number};

    # Add <mtapp:fields> before tags
    populate_field_loop($cb, $app, $param, $tmpl, $obj);

    my $content_fields = $tmpl->getElementById('content_fields');
    my $beacon_tmpl = File::Spec->catdir($commercial->path, 'tmpl', 'field_beacon.tmpl');
    my $beacon = $tmpl->createElement('include', { name => $beacon_tmpl });
    $tmpl->insertAfter($beacon, $content_fields);

    # # Finally display our reorder widget
    add_reorder_widget($cb, $app, $param, $tmpl);

    1;
}

sub populate_field_loop {
    my ($cb, $app, $param, $tmpl, $obj) = @_;
    my $plugin = $cb->plugin;
    my $q = $app->param;


    my $mode = $app->mode;
    my $blog_id = $q->param('blog_id');
    my $object_id = $q->param('id');
    my $object_type = $param->{asset_type} || $q->param('_type');
    my $is_entry = ($object_type eq 'entry' || $object_type eq 'page' || $mode eq 'cfg_entry');
    my $label_class = delete $param->{label_class}
        if exists $param->{label_class};
    my $field_type = $param->{field_type};
    my %param      = (
        $blog_id ? ( blog_id => $blog_id ) : (),
        ( $mode eq 'cfg_entry' )
        ? ( object_type => $field_type )
        : ( object_type => $object_type, object_id => $object_id ),
        $label_class ? ( label_class => $label_class ) : (),
        params => $param,
        entry => $obj,
    );
    my $prefix    = $field_type ? $field_type . '_' : '';
    my $loop_name = $prefix . 'field_loop';
    my $loop      = $param->{$loop_name};
    my $fields    = field_loop_workflow(%param);
    foreach my $field (@$fields) {
        my $basename = $field->{basename};
        unless ( exists $field->{show_field} ) {
            my $show =
               !$is_entry                                                     ? 1
              : $field->{required}                                            ? 1
              : $param->{ $prefix . "disp_prefs_show_customfield_$basename" } ? 1
              :                                                                 0;
            $field->{show_field} = $show;
        }
        $field->{use_field} = $field->{required} ne ''
          if !exists $field->{use_field};    # TODO: created by user
        $field->{lock_field} = 1 if $field->{required};
    }

    my %locked_fields;
    if ( $loop ) { # an existing field loop, merge our fields into it
        my $i = 100;
        FIELD: foreach my $field ( @$loop ) {
            if ( $field->{field_name} =~ m/^field\.(.+)$/ ) {
                $field->{field_name} = 'customfield_' . $1;
                $field->{field_id} = 'customfield_' . $1;
                $field->{field_order} = $i++
                    unless exists $field->{field_order};
            }
            $locked_fields{$field->{field_id}} = 1
                if $field->{lock_field};
            foreach my $cf ( @$fields ) {
                if ( $cf->{field_name} eq $field->{field_name} ) {
                    foreach (keys %$cf) {
                        $field->{$_} = $cf->{$_} unless exists $field->{$_};
                    }
                    $cf->{remove} = 1;
                    next FIELD;
                }
                $cf->{field_order} = $i++ unless exists $cf->{field_order};
            }
        }
        @$fields = grep { !$_->{remove} } @$fields;
        push @$loop, @$fields;
    }
    else {
        $loop = $param->{$loop_name} = $fields;
        my $i = 0;
        foreach (@$loop) {
            $_->{field_order} = $i++;
        }
    }

    if ( my $cf_loop = $param->{ $prefix . 'disp_prefs_custom_fields' } ) {
        # user supplied sort order
        my %order;
        my $i = 200;
        foreach (@$cf_loop) {
            $order{$_->{name}} = $i++
                unless exists $locked_fields{$_->{name}};
        }

        no warnings;
        @$loop = sort { ($order{$a->{field_id}} || $a->{field_order} || 0)
            <=> ($order{$b->{field_id}} || $b->{field_order} || 0) } @$loop;
    }
}

sub field_loop_workflow {
    my $app = MT->instance;
    my (%param) = @_;

    my $blog_id = $param{blog_id} || 0;
    my $obj_type = $param{object_type};
    my $id = $param{object_id};
    my $simple = $param{simple} || 0;
    my $label_class = $param{label_class} || undef;
    my $params = $param{params} || {};
    my $pre_loaded = delete($params->{__fields_pre_loaded__}) || 0;

    return '' if !$obj_type;
    my $obj_class = MT->model($obj_type);

    my ($obj, $meta_data, @pre_sort, %markers, @post_sort, $out);

    # TODO: does this really need to be not-reedit only?
    if ( $id
      && !$app->param('reedit')
      && !$pre_loaded )
    {
#        my $reviewee = $app->can_do('request_entry_approval');
#        my $reviewer = $app->can_do('approve_entry');
#        if ($reviewer || $reviewee) {
            $meta_data = get_meta($param{entry});
#        } else {
#            $obj = $obj_class->load($id);
#            $meta_data = get_meta($obj);
#        }
    }

    my $q = $app->param;
    my %date_fields;
    for my $form_field ($q->param()) {
        if ($form_field =~ m/^([td]_)?customfield_(.*?)$/) {
            my ($td, $field_name) = ($1, $2);
            if ($td) {
                $date_fields{$field_name} = 1;
            }
            else {
                if ($form_field =~ m/^customfield_(.*?)_cb_beacon$/) {
                    $field_name = $1;
                    if ( $q->param($form_field) && !$q->param('customfield_' . $field_name) ) {
                        $meta_data->{$field_name} = 0;
                    }
                }
                else {
                    $meta_data->{$field_name} = $q->param($form_field);
                }
            }
        }
    }

    for my $field_name (keys %date_fields) {
        $meta_data->{$field_name} = {
            'date' => $q->param("d_customfield_$field_name"),
            'time' => $q->param("t_customfield_$field_name"),
        };
    }

    my $terms = {
        obj_type => $obj_type,
        $blog_id ? ( blog_id => [ $blog_id, 0 ] ) : ()
    };

    my %show_field_map = ();
    if ( $obj && $obj->isa('MT::Entry') ) {
        my $cats = $obj->categories;
        foreach my $cat ( @$cats ) {
            my $fields = $cat->show_fields;
            $show_field_map{$_} = 1 foreach split /,/, $fields;
        }
    }

    require CustomFields::Field;
    my $iter = CustomFields::Field->load_iter($terms);
    while (my $field = $iter->()) {
        my ($id, $type, $basename) = ($field->id, $field->type, $field->basename);
        my $type_obj = MT->registry('customfield_types', $type);

        my $row = $field->get_values();
        $row->{field_label} = $row->{name};
        $row->{blog_id} ||= ($obj && $obj_class->has_column('blog_id')) ? $obj->blog_id : 0;
        $row->{value} = $pre_loaded
            ? $params->{ 'field.' . $field->basename }
            : ( $meta_data && defined( $meta_data->{$basename} ) )
                ? $meta_data->{$basename}
                : $field->default;

        # If an options_delimiter is present, we need to populate an option_loop
        if($type_obj->{options_delimiter}) {
            my @option_loop;
            my $expr = '\s*' . quotemeta($type_obj->{options_delimiter}) . '\s*';
            my @options = split /$expr/, $field->options;
            foreach my $option (@options) {
                my $label = $option;
                if ($option =~ m/=/) {
                    ($option, $label) = split /\s*=\s*/, $option, 2;
                }
                my $option_row = { option => $option, label => $label };
                $option_row->{is_selected} = defined $row->{value} ? ($row->{value} eq $option) : 0;
                push @option_loop, $option_row;
            }
            $row->{option_loop} = \@option_loop;
        }

        if ( ( $field->obj_type eq 'entry' ) || ( $field->obj_type eq 'page' ) ) {
            if ( exists($show_field_map{$field->id}) && $show_field_map{$field->id} ) {
                $row->{show_field} = 1;
            }
        } else {
            $row->{show_field} = 1;
        }
        $row->{show_hint} = $type ne 'checkbox' ? 1 : 0;
        $row->{content_class} = $type =~ m/radio|checkbox/ ? 'field-content-text' : '';

        # Now build the field_content using field_html
        $row->{field_id} = $row->{field_name} = "customfield_$basename";
        if ( $row->{type} eq 'datetime' ) {
            my $blog = $app->blog;
            my $ts = $row->{value};
            if ( $ts ) {
                if ( $row->{options} eq 'datetime' ) {
                    $row->{field_value} = format_ts( "%x %X", $ts, $blog, $app->user ? $app->user->preferred_language : undef );
                }
                elsif ( $row->{options} eq 'date' ) {
                    $row->{field_value} = format_ts( "%x", $ts, $blog, $app->user ? $app->user->preferred_language : undef );
                }
                elsif ( $row->{options} eq 'time' ) {
                    $row->{field_value} = format_ts( "%H:%M:%S", $ts, $blog, $app->user ? $app->user->preferred_language : undef );
                }
            } else {
                $row->{field_value} = '';
            }
        }
        else {
            $row->{field_value} = $row->{value};
        }
        $row->{simple} = $simple if $simple;
        $row->{label_class}
            = $label_class ? $label_class
            : (    $field->obj_type eq 'author'
                || $field->obj_type eq 'comment'
                || $field->obj_type eq 'category'
                || $field->obj_type eq 'folder' ) ? 'left-label'
            : 'top-label';
        $row->{field_html} = _get_html($type, 'field_html', { %$params, %$row } );

        push @pre_sort, $row;
    }

    # Populate where the fields are in @pre_sort
    for (my $i = 0; $i < scalar @pre_sort; $i++) {
        my $basename = $pre_sort[$i]->{basename};
        $markers{$basename} = $i;
    }
    return \@pre_sort;

}

sub add_reorder_widget {
    my ($cb, $app, $param, $tmpl) = @_;
    my $plugin = MT->component('commercial');

    # Get our header include using the DOM
    my ($header);
    my $includes = $tmpl->getElementsByTagName('include');
    foreach my $include (@$includes) {
        if($include->attributes->{name} =~ /header.tmpl$/) {
            $header = $include;
            last;
        }
    }

    return 1 unless $header;

    require MT::Template;
    bless $header, 'MT::Template::Node';

    require File::Spec;
    my $reorder_widget_tmpl = File::Spec->catdir($plugin->path,'tmpl','reorder_fields.tmpl');
    my $reorder_widget = $tmpl->createElement('include', { name => $reorder_widget_tmpl });

    $tmpl->insertBefore($reorder_widget, $header);
}

sub add_status_for_revision {
    my ($cb, $app, $template) = @_;

    my $old;
    my $new;

    $old = <<HTML;
<mt:var name="pager" value="\$hide_pager" default="0">
HTML
    $old = quotemeta($old);

    $new = <<HTML;
<style type="text/css">
.list-revision-dialog .col.status {
    width: 22%;
}
</style>
<mt:var name="pager" value="\$hide_pager" default="0">
HTML

    $$template =~ s/$old/$new/;


    $old = <<HTML;
      <mt:else name="rev_status" eq="5">
        <span class="icon-left-wide icon-spam"><__trans phrase="Spam"></span>
HTML
    $old = quotemeta($old);

    my $plugin = MT->component('PublishDraft');
    my $status_publish_draft =  $plugin->translate('Published (Draft)');
    my $status_publish_future =  $plugin->translate('Published (Future)');

    $new = <<HTML;
      <mt:else name="rev_status" eq="5">
        <span class="icon-left-wide icon-spam"><__trans phrase="Spam"></span>
      <mt:else name="rev_status" eq="10">
        <span class="icon-left-wide2 icon-success-draft"> $status_publish_draft</span>
      <mt:else name="rev_status" eq="100">
        <span class="icon-left-wide2 icon-success-future"> $status_publish_future</span>
HTML
    $$template =~ s/$old/$new/;

}

sub add_css {
    my ($cb, $app, $template) = @_;

    my $old;
    my $new;

    $old = <<HTML;
<mt:var name="html_head">
HTML
    $old = quotemeta($old);

    $new = <<HTML;
<mt:setvarblock name="html_head" append="1">
<style type="text/css">
.icon-left-wide2 {
    background-position: left center;
    background-repeat: no-repeat;
    padding-left: 25px;
}
/* ################################## typo ############################### */
.icon-success-draft {
    background-image: url("<mt:staticwebpath>plugins/PublishDraft/images/publish_draft.gif");
}
.icon-success-future {
    background-image: url("<mt:staticwebpath>plugins/PublishDraft/images/publish_future.gif");
}
/* ################################## typo ############################### */
.listing .status-publish-draft img {
    background-image: url("<mt:staticwebpath>plugins/PublishDraft/images/publsih_draft.gif");
}

/* Blog Stats */
.blog-stats-widget .entry-status-publish-draft {
    background-image:url("<mt:staticwebpath>plugins/PublishDraft/images/publsih_draft.gif");
}
.blog-stats-widget .content.entry-status-publish-draft {
    padding-left: 33px;
}
</style>
</mt:setvarblock>
<mt:var name="html_head">
HTML

    $$template =~ s/$old/$new/;
}

sub add_status_for_list {
    my $cb = shift;
    my ( $app, $res, $objs ) = @_;

    my $type = 'entry';

    for my $data (@{$res->{objects}}) {
        my $model = $app->model($type);
        my $rev_model = $app->model($type.':revision');
        return 1 unless $rev_model;

        my ($terms, $args);
        $terms->{'entry_id'} = $data->[0];
        $args->{sort} = 'rev_number';
        $args->{direction} = 'descend';
        $args->{limit} = 1;
        my @revs = $rev_model->load($terms, $args);
        my $revision = shift @revs || undef;

        my $entry;
        if ($revision) {
            my @objs = $model->load($data->[0]);
            my $obj = shift @objs || undef;
            my $rev = $obj->load_revision( { rev_number => $revision->rev_number } );
            if ( $rev && @$rev ) {
                $entry = $rev->[0];
            }
        }
        if ($entry) {
############################################### typo #####################################
            for (my $i=1; $i<=2; $i++) {
                if ($entry->status == 10) {
                    $data->[$i] =~ s!/images/status_icons/"!/plugins/PublishDraft/images/publish_draft.gif"!;
                    $data->[$i] =~ s!/images/status_icons/success\.gif!/plugins/PublishDraft/images/publish_draft.gif!;
                }
                if ($entry->status == 100) {
                    $data->[$i] =~ s!/images/status_icons/"!/plugins/PublishDraft/images/publish_future.gif"!;
                    $data->[$i] =~ s!/images/status_icons/success\.gif!/plugins/PublishDraft/images/publish_future.gif!;
                }
            }
###############################################
        }
    }
}

sub add_status {
    my ($cb, $app, $template) = @_;

    my $plugin = MT->component('PublishDraft');
    my $publish_status_draft = $plugin->translate('Published (Draft)');
##################
    my $publish_status_future = $plugin->translate('Published (Future)');
##################

    my $old;
    my $new;

    $old = <<HTML;
  <mt:unless name="new_object">
    <mt:if name="status_draft">
    <span class="icon-left-wide icon-draft"><__trans phrase="Unpublished (Draft)"></span>
    <mt:else name="status_review">
    <span class="icon-left-wide icon-warning"><__trans phrase="Unpublished (Review)"></span>
    <mt:else name="status_future">
    <span class="icon-left-wide icon-future"><__trans phrase="Scheduled"></span>
    <mt:else name="status_publish">
    <span class="icon-left-wide icon-success"><__trans phrase="Published"></span>
    <mt:else name="status_spam">
    <span class="icon-left-wide icon-spam"><__trans phrase="Unpublished (Spam)"></span>
    <mt:else name="status_unpublish">
    <span class="icon-left-wide icon-unpublish"><__trans phrase="Unpublished (End)"></span>
    </mt:if>
  </mt:unless>
HTML
    $old = quotemeta($old);

    $new = <<HTML;
  <mt:unless name="new_object">
    <mt:if name="status_draft">
    <span class="icon-left-wide icon-draft"><__trans phrase="Unpublished (Draft)"></span>
    <mt:else name="status_review">
    <span class="icon-left-wide icon-warning"><__trans phrase="Unpublished (Review)"></span>
    <mt:else name="status_future">
    <span class="icon-left-wide icon-future"><__trans phrase="Scheduled"></span>
    <mt:else name="status_publish_draft">
    <span class="icon-left-wide2 icon-success-draft">$publish_status_draft</span>
<mt:ignore>####################################</mt:ignore>
    <mt:else name="status_publish_future">
    <span class="icon-left-wide2 icon-success-future">$publish_status_future</span>
<mt:ignore>####################################</mt:ignore>
    <mt:else name="status_publish">
    <span class="icon-left-wide icon-success"><__trans phrase="Published"></span>
    <mt:else name="status_spam">
    <span class="icon-left-wide icon-spam"><__trans phrase="Unpublished (Spam)"></span>
    <mt:else name="status_unpublish">
    <span class="icon-left-wide icon-unpublish"><__trans phrase="Unpublished (End)"></span>
    </mt:if>
  </mt:unless>
HTML
    $$template =~ s/$old/$new/;

    $old = <<HERE;
  <div id="quickpost">
    <p><mt:var name="quickpost_js"></p>
  </div>
HERE
    $old = quotemeta($old);
    $new = <<HERE;
  <div id="quickpost">
    <p><mt:var name="quickpost_js"></p>
  </div>
<div style="margin-top:20px">
<mt:var name="rest_day">
</div>
HERE
        $$template =~ s!$old!$new!;

    $old = <<HERE;
    <mt:setvar name="publish_button_text" value="<__trans phrase="Publish">">
    <mt:setvarblock name="publish_button_title"><__trans phrase="Publish this [_1]" params="<mt:var name="object_label">" escape="html"></mt:setvarblock>
HERE
    $old = quotemeta($old);
    $new = <<HERE;
    <mt:setvar name="publish_button_text" value="<__trans phrase="Publish">">
    <mt:setvarblock name="publish_button_title"><__trans phrase="Publish this [_1]" params="<mt:var name="object_label">" escape="html"></mt:setvarblock>
    <mt:setvar name="update_button_text" value="<__trans phrase="Update">">
    <mt:setvarblock name="update_button_title"><__trans phrase="Update this [_1]" params="<mt:var name="object_label">" escape="html"></mt:setvarblock>
HERE

    $$template =~ s!$old!$new!g;

    $old = <<HTML;
</mt:if>
<mt:if name="loaded_revision">
HTML
    $old = quotemeta($old);

    $new = <<HTML;
</mt:if>
<mt:if name="loaded_revision">
  <mt:if name="status_publish_draft">
    <mt:var name="current_status_text" value="success-draft icon-left-wide2">
    <mt:setvarblock name="current_status_label" value="">$publish_status_draft</mt:setvarblock>
  </mt:if>
HTML
# no change
#    $$template =~ s/$old/$new/;

    $old = <<HTML;
<mt:setvarblock name="related_content">
HTML
    $old = quotemeta($old);

    $new = <<HTML;
<mt:setvarblock name="related_content" append="1">
<mtapp:widget
   id="entry-status-widget"
   class="status-widget"
   label="<__trans phrase="Status">">
  <mt:if name="new_object">
  <mt:else>
  <div class="revision-info">
      <mt:if name="rev_date">
    <p><__trans phrase="Revision: <strong>[_1]</strong>" params="<mt:var name="rev_date" escape="html">"></p>
    <a href="<mt:var name="script_url">?__mode=list_revision2&_type=<mt:var name="object_type" default="link">&id=<mt:var name="id" escape="url">&blog_id=<mt:var name="blog_id">&r=<mt:var name="rev_number" escape="html">" class="mt-open-dialog" title="<__trans phrase="View revisions of this [_1]" params="<mt:var name="object_label">">"><__trans phrase="View revisions"></a>
      <mt:else>
    <p class="zero-state"><__trans phrase="No revision(s) associated with this [_1]" params="<mt:var name="object_label" lower_case="1" escape="html">"></p>
      </mt:if>
  </div>
  <ul>
    <li><__trans phrase="[_1] - Created by [_2]" params="<mt:date ts="\$created_on_ts" relative="1" _default="\$created_on_formatted" format="%b %e %Y">%%<mt:var name="created_by" escape="html" escape="html">"></li>
      <mt:if name="status_publish">
    <li><__trans phrase="[_1] - Published by [_2]" params="<MTDate ts="\$authored_on_ts" relative="1" _default="\$authored_on_formatted" format="%b %e %Y">%%<mt:var name="author_name" escape="html" escape="html">"></li>
      </mt:if>
      <mt:if name="modified_by">
    <li><__trans phrase="[_1] - Edited by [_2]" params="<MTDate ts="\$modified_on_ts" relative="1" _default="\$modified_on_formatted" format="%b %e %Y">%%<mt:var name="modified_by" escape="html" escape="html">"></li>
      </mt:if>
  </ul>
  </mt:if>
</mtapp:widget>
</mt:setvarblock>
<mt:setvarblock name="related_content">
HTML
    $$template =~ s/$old/$new/;

    my $reviewee = $app->can_do('request_entry_approval');
    my $reviewer = $app->can_do('approve_entry');

    $old = <<HTML;
<div id="msg-block">
HTML
    $old = quotemeta($old);

    my $publish_draft = $plugin->translate('Publish Draft.');
######################
    my $publish_future = $plugin->translate('Publish Future.');
######################

    $new = <<HTML;
<div id="msg-block">
<mt:if name="saved_snapshot">
<mt:if name="status_publish_draft">
  <mtapp:statusmsg
     id="status_publish_draft"
     class="success">
     $publish_draft
  </mtapp:statusmsg>
</mt:if>
<mt:ignore>######################</mt:ignore>
<mt:if name="status_publish_future">
  <mtapp:statusmsg
     id="status_publish_future"
     class="success">
     $publish_future
  </mtapp:statusmsg>
</mt:if>
<mt:ignore>######################</mt:ignore>
</mt:if>

HTML
    $$template =~ s/$old/$new/;


        $old = <<HERE;
    <select name="status" id="status" class="full" onchange="highlightSwitch(this)">
      <option value="1"<mt:if name="status_draft"> selected="selected"</mt:if>><__trans phrase="Unpublished (Draft)"></option>
      <option value="2"<mt:if name="status_publish"> selected="selected"</mt:if>><__trans phrase="Published"></option>
    <mt:if name="status_review">
      <option value="3" selected="selected"><__trans phrase="Unpublished (Review)"></option>
    <mt:else name="status_spam">
      <option value="5" selected="selected"><__trans phrase="Unpublished (Spam)"></option>
    </mt:if>
    <mt:if name="new_object">
      <option value="4"<mt:if name="status_future"> selected="selected"</mt:if>><__trans phrase="Scheduled"></option>
    <mt:else>
      <mt:unless name="status_publish">
      <option value="4"<mt:if name="status_future"> selected="selected"</mt:if>><__trans phrase="Scheduled"></option>
      </mt:unless>
    </mt:if>
    <mt:if name="status_unpublish">
      <option value="6" selected="selected"><__trans phrase="Unpublished (End)"></option>
    </mt:if>
    </select>
HERE

    $old = quotemeta($old);
    $new = <<HERE;
<mt:ignore>Status:<mt:getvar name="status"></mt:ignore>
<__trans_section component="PublishDraft">
    <select name="status" id="status" class="full" onchange="highlightSwitch(this)">
      <option value="1"<mt:if name="status_draft"> selected="selected"</mt:if>><__trans phrase="Unpublished (Draft)"></option>
      <option value="2"<mt:if name="status_publish"> selected="selected"</mt:if>><__trans phrase="Published"></option>
    <mt:if name="status_draft">
        <option value="4"><__trans phrase="Scheduled"></option>
    <mt:else name="status_future">
        <option value="4" selected="selected"><__trans phrase="Scheduled"></option>
    <mt:else name="status_publish_draft">
      <option value="10" selected="selected"><__trans phrase="Published (Draft)"></option>
      <option value="100"><__trans phrase="Published (Future)"></option>
    <mt:else name="status_publish">
      <mt:if name="new_object">
        <option value="4"<mt:if name="status_future"> selected="selected"</mt:if>><__trans phrase="Scheduled"></option>
      <mt:else>
        <option value="10" selected="selected"><__trans phrase="Published (Draft)"></option>
        <option value="100"<mt:if name="status_publish_future"> selected="selected"</mt:if>><__trans phrase="Published (Future)"></option>
      </mt:if>
<mt:ignore>################################</mt:ignore>
    <mt:else name="status_publish_future">
        <option value="10"><__trans phrase="Published (Draft)"></option>
        <option value="100" selected="selected"><__trans phrase="Published (Future)"></option>
<mt:ignore>################################</mt:ignore>
    </mt:if>
    <mt:if name="status_unpublish">
      <option value="6" selected="selected"><__trans phrase="Unpublished (End)"></option>
    </mt:if>
    </select>
</__trans_section>
HERE
    $$template =~ s!$old!$new!;

    $old = <<HERE;
    function setSaveButtonText () {
        var selectedStatus = jQuery('select#status').val();
        if (selectedStatus == 2) {
            jQuery('button.publish').text('<mt:var name="publish_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="publish_button_title">');
        } else if (selectedStatus == 4) {
            jQuery('button.publish').text('<mt:var name="scheduled_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="scheduled_button_title">');
        } else {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="draft_button_title">');
        }
    }
HERE

    $old = quotemeta($old);
    $new = <<HERE;
    function setSaveButtonText () {
        var selectedStatus = jQuery('select#status').val();
        if (selectedStatus == 3) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="draft_button_title">');
        } else if (selectedStatus == 4) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="scheduled_button_title">');
// ###########################################
        } else if (selectedStatus == 100) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="scheduled_button_title">');
// ###########################################
        } else if (selectedStatus == 10) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="draft_button_title">');
        } else if (selectedStatus == 20) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="draft_button_title">');
        } else if (selectedStatus == 1) {
            jQuery('button.publish').text('<mt:var name="draft_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="draft_button_title">');
        } else {
            jQuery('button.publish').text('<mt:var name="publish_button_text">');
            jQuery('button.publish').attr('title', '<mt:var name="publish_button_title">');
        }
    }

  <mt:unless name="config.previewinnewwindow">
    jQuery('button.publish').click(function(event) {
        var selectedStatus = jQuery('select#status').val();
        if (selectedStatus == 10 || selectedStatus == 20 || selectedStatus == 100) {
            jQuery('form#entry_form input[name=__mode]').val('save_snapshot_wf');
        }
    });
  </mt:unless>

    jQuery('#status').bind('change', function() {
        var changedStatus = jQuery('#status option:selected').val();
    });
HERE
        $$template =~ s!$old!$new!;

    $old = <<HERE;
    jQuery('button.publish').click(function(event) {
        app.clearDirty();
        app.stopAutoSave();
        jQuery('form#entry_form input[name=__mode]').val('save_entry');
        jQuery('form#entry_form').removeAttr('target');
        jQuery(this).attr('disabled', 'disabled');
        jQuery('form#entry_form').submit();
    });
HERE

    $old = quotemeta($old);
    $new = <<HERE;
    jQuery('button.publish').click(function(event) {
        app.clearDirty();
        app.stopAutoSave();
        jQuery('form#entry_form input[name=__mode]').val('save_entry');
        var selectedStatus = jQuery('select#status').val();
        if (selectedStatus == 10 || selectedStatus == 20 || selectedStatus == 100) {
            jQuery('form#entry_form input[name=__mode]').val('save_snapshot_wf');
        }
        jQuery('form#entry_form').removeAttr('target');
        jQuery(this).attr('disabled', 'disabled');
        jQuery('form#entry_form').submit();
    });
HERE
        $$template =~ s!$old!$new!;

    # for MT6.1
    $old = <<HERE;
    jQuery('button.publish').click(function(event) {
        jQuery('form#entry_form input[name=__mode]').val('save_entry');
        jQuery('form#entry_form').removeAttr('target');
        jQuery('form#entry_form').attr('mt:once', '1');
    });
HERE

    $old = quotemeta($old);
    $new = <<HERE;
    jQuery('button.publish').click(function(event) {
        jQuery('form#entry_form input[name=__mode]').val('save_entry');
        jQuery('form#entry_form').removeAttr('target');
        jQuery('form#entry_form').attr('mt:once', '1');
        var selectedStatus = jQuery('select#status').val();
        if (selectedStatus == 10 || selectedStatus == 20 || selectedStatus == 100) {
            jQuery('form#entry_form input[name=__mode]').val('save_snapshot_wf');
        }
    });
HERE
        $$template =~ s!$old!$new!;
}

sub itemset_action_widget {
    my ($cb, $app, $template) = @_;

    my $q = $app->{query};
    my $blog_id = $q->param('blog_id');

    my $plugin = MT->component('PublishDraft');
    my $workflow_display_unpublish_option = $plugin->get_config_value('workflow_display_unpublish_option', 'blog:' . $blog_id);
    my $reviewee = $app->can_do('request_entry_approval');

    if ($reviewee && !$workflow_display_unpublish_option) {

    my $old = <<HERE;
<select name="plugin_action_selector" onchange="updatePluginAction(this)">
    <option value="0"><__trans phrase="More actions..."></option>
    <mt:loop name="list_actions">
    <option value="<mt:var name="key">"><mt:var name="label"></option>
    </mt:loop>
    <mt:loop name="more_list_actions">
        <mt:if name="__first__">
    <optgroup label="<__trans phrase='Plugin Actions'>">
        </mt:if>
        <option value="<mt:var name="key">"><mt:var name="label"></option>
        <mt:if name="__last__">
    </optgroup>
        </mt:if>
    </mt:loop>
</select>
<mt:if name="form_id">
<button
     class="small-button mt-<mt:var name="form_id">-action"
     ><__trans phrase="Go"></button>
<mt:else>
<button
     class="small-button mt-<mt:var name="object_type">-listing-form-action"
     ><__trans phrase="Go"></button>
</mt:if>
HERE
    $old = quotemeta($old);

    $$template =~ s!$old!!;

    }
}


1;
