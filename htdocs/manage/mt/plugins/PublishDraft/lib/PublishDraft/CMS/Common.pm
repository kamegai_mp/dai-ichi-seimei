package PublishDraft::CMS::Common;

use strict;
use warnings;

sub blog_stats_entry {
    my ($cb, $app, $template) = @_;

    my $old;
    my $new;

    $old = <<HTML;
  <div id="entry-<mt:entryid>" class="entry content entry-<mt:if name="__odd__">odd odd<mt:else>even even</mt:if> entry-status-<mt:entrystatus lower_case="1"><mt:if name="entry_type"> entry-<mt:var name="entry_type"></mt:if>">
HTML
    $old = quotemeta($old);

    $new = <<HTML;
  <div id="entry-<mt:entryid>" class="entry content entry-<mt:if name="__odd__">odd odd<mt:else>even even</mt:if> entry-status-<mt:EntryStatusForPublishDraft lower_case="1"><mt:if name="entry_type"> entry-<mt:var name="entry_type"></mt:if>">
HTML

    $$template =~ s/$old/$new/;
}

1;
