package PublishDraft::Util;

use strict;
use warnings;

use Exporter;
@PublishDraft::Util::ISA = qw( Exporter );
use vars qw( @EXPORT_OK );
################################################
@EXPORT_OK = qw( get_lastest_revision get_lastest_revision_object get_lastest_revision_for_future_post get_lastest_revision_object_for_future_post );

sub get_lastest_revision_for_future_post {
    my ($app, $type, $id) = @_;

    my ($terms, $args);
    $terms->{entry_id} = $id;
    $args->{sort} = 'rev_number';
    $args->{direction} = 'descend';
    $args->{limit} = 1;
    my $rev_model = $app->model($type.':revision');
    my @revs = $rev_model->load($terms, $args);
    my $revision = shift @revs || undef;

    # for upgrade from without revision function.
    if ( !defined $revision ){
        my $entry = MT::Entry->load($id);
        return undef if !$entry;

        my @changed_cols;
        my $revisioned_cols = $entry->revisioned_columns;
        foreach my $col (@$revisioned_cols) {
            push @changed_cols, $col;
        }
        $entry->{changed_revisioned_cols} = \@changed_cols;
        my $revision_number = $entry->save_revision('for Workflow');

        @revs = $rev_model->load($terms, $args);
        $revision = shift @revs || undef;
    }

    return $revision;
}

sub get_lastest_revision_object_for_future_post {
    my ($app, $type, $id, $revision) = @_;

    my @objs = $app->model($type)->load($id);
    my $obj = shift @objs || undef;
    return $app->error( "No Object." ) if !$obj;

    my $rev = $obj->load_revision( { rev_number => $revision->rev_number } );
    if ( $rev && @$rev ) {
        $obj = $rev->[0];
    }
}

################################################

sub get_lastest_revision {
    my $app = shift;

    my $q    = $app->param;
    my $type = $q->param('_type');
    my $id   = $q->param('id');

    my ($terms, $args);
    $terms->{entry_id} = $id;
    $args->{sort} = 'rev_number';
    $args->{direction} = 'descend';
    $args->{limit} = 1;
    my $rev_model = $app->model($type.':revision');
    my @revs = $rev_model->load($terms, $args);
    my $revision = shift @revs || undef;

    # for upgrade from without revision function.
    if ( !defined $revision ){
        my $entry = MT::Entry->load($id);
        return undef if !$entry;

        my @changed_cols;
        my $revisioned_cols = $entry->revisioned_columns;
        foreach my $col (@$revisioned_cols) {
            push @changed_cols, $col;
        }
        $entry->{changed_revisioned_cols} = \@changed_cols;
        my $revision_number = $entry->save_revision('for Workflow');

        @revs = $rev_model->load($terms, $args);
        $revision = shift @revs || undef;
    }

    return $revision;
}

sub get_lastest_revision_object {
    my ($app, $revision) = @_;

    my $q    = $app->param;
    my $type = $q->param('_type'); #'entry';
    my $id   = $q->param('id');

    my @objs = $app->model($type)->load($id);
    my $obj = shift @objs || undef;
    return $app->error( "No Object." ) if !$obj;

    my $rev = $obj->load_revision( { rev_number => $revision->rev_number } );
    if ( $rev && @$rev ) {
        $obj = $rev->[0];
    }
}

1;
