(function($){

if(blogID=='3'){
	$('#menu-entry .top-menu-link > span').text('料金編集');
	$('#menu-page .top-menu-link > span').text('PDF登録');
	$('#text-field').css('display','none');
	$('#extra_path').val('pdf');
}

if(blogID=='4'){
	$('#menu-entry .top-menu-link > span').text('設備登録');
	$('#menu-page').css('display','none');
	$('#text-field').css('display','none');

}

if(blogID=='2'){

	var config = MT.Editor.TinyMCE.config;
	$.extend(config, {
    	forced_root_block : ''
	});

	$('#menu-entry .top-menu-link > span').text('お知らせ登録');
	$('#menu-page').css('display','none');
	$('#extra_path').val('pdf');
}

if(blogID=='5'){
	$('#menu-entry .top-menu-link > span').text('スケジュール登録');
	$('#menu-page').css('display','none');
	$('#text-field').css('display','none');
	$('#extra_path').val('items');

	var config = MT.Editor.TinyMCE.config;
	$.extend(config, {
    	forced_root_block : ''
	});

    if (mtappVars.template_filename == 'edit_entry' && $('#excerpt').length ) {
        var excerpt_mce = new MT.EditorManager('excerpt');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }
	
	//各カスタムフィールドにTinyMCEを適用

    if (mtappVars.template_filename == 'edit_entry' && $('#customfield_cf_sch_sub').length ) {
        var excerpt_customfield_cf_sch_sub = new MT.EditorManager('customfield_cf_sch_sub');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }

    if (mtappVars.template_filename == 'edit_entry' && $('#customfield_cf_sch_act_').length ) {
        var excerpt_customfield_cf_sch_sub = new MT.EditorManager('customfield_cf_sch_act_');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }

    if (mtappVars.template_filename == 'edit_entry' && $('#customfield_cf_sch_rmk_').length ) {
        var excerpt_customfield_cf_sch_sub = new MT.EditorManager('customfield_cf_sch_rmk_');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }

    if (mtappVars.template_filename == 'edit_entry' && $('#customfield_cf_sch_inq_ex').length ) {
        var excerpt_customfield_cf_sch_sub = new MT.EditorManager('customfield_cf_sch_inq_ex');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }

    if (mtappVars.template_filename == 'edit_entry' && $('#customfield_cf_sch_info').length ) {
        var excerpt_customfield_cf_sch_sub = new MT.EditorManager('customfield_cf_sch_info');
        $('#entry_form').submit(function() {
            excerpt_mce.currentEditor.save();
        });
    }

}

    $.MTAppDebug();
})(jQuery);

